﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using OxyPlot;
using OxyPlot.Wpf;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Annotations;


namespace VMS.TPS
{
    public class Script 
    {
        public Script()
        {
        }

        // dummy objects to force load of OxyPlot packages
        private static readonly PlotModel plotModel = new PlotModel();
        private static readonly PlotView plotView = new PlotView();
        private static readonly ColumnItem columnItem = new ColumnItem();
        private static readonly OxyPlot.Axes.CategoryAxis ax = new OxyPlot.Axes.CategoryAxis();
        private static readonly OxyPlot.Annotations.LineAnnotation line = new OxyPlot.Annotations.LineAnnotation();
        public void Execute(ScriptContext context, System.Windows.Window window)
        {
            PlanSetup plan = context.PlanSetup;
            var viewModel = new RectumDirectionalGradientManyViews.ViewModels.MainViewModel(context);
            var mainView = new RectumDirectionalGradientManyViews.MainView(viewModel);
            window.Title = "Directional Dose Gradient";
            window.Content = mainView;
        }

        
    }
}
