﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace RectumDirectionalGradientManyViews
{
    public static class CommonMethods
    {
        //Add bars showing where ROIs are located
        public static void AddStructureBarAnnotation(PlotModel pm, PlanSetup plan, string[] structureIDs, OxyColor[] oxyColors)
        {
            int idx = 0;
            int colorIdx = 0;
            IntervalBarSeries barSeries = new OxyPlot.Series.IntervalBarSeries
            {
                LabelMargin = 0,
                BarWidth = 1.5
            };

            foreach (var structureID in structureIDs)
            {
                if (plan.StructureSet.Structures.Any(s => (s.Id.ToUpper() == structureID.ToUpper() & !s.IsEmpty)))
                {
                    var structurePlanes = FindPlanesWithStructure(plan, structureID);
                    var oxyColor = OxyColors.Gray; // default color
                                                   // if enough colors are given set to other color
                    if (oxyColors.Length >= idx + 1)
                    {
                        oxyColor = oxyColors[colorIdx];
                    }


                    // create bar item for the structure
                    IntervalBarItem structLabelBar = new IntervalBarItem
                    {
                        Start = (plan.Dose.Origin.z + structurePlanes[0] * plan.Dose.ZRes) / 10.0,
                        End = (plan.Dose.Origin.z + structurePlanes[structurePlanes.Count() - 1] * plan.Dose.ZRes) / 10.0,
                        CategoryIndex = idx,
                        Color = oxyColor,
                        Title = ""
                    };

                    barSeries.Items.Add(structLabelBar);

                    // add veritcal lines showing where the structure starts and ends
                    AddVerticalAnnotation(pm, plan, "", oxyColor, (plan.Dose.Origin.z + structurePlanes[0] * plan.Dose.ZRes) / 10.0);
                    AddVerticalAnnotation(pm, plan, "", oxyColor, (plan.Dose.Origin.z + structurePlanes[structurePlanes.Count() - 1] * plan.Dose.ZRes) / 10.0);
                    idx++;
                }
                colorIdx++;
            }

            // add to plotmodel
            barSeries.YAxisKey = "Hidden YAxis";
            barSeries.Tag = plan.Id;
            pm.Series.Add(barSeries);

        }

        // Add vertical line showing where tructure starts or ends
        private static void AddVerticalAnnotation(PlotModel pm, PlanSetup plan, string text, OxyColor color, double x)
        {
            pm.Annotations.Add(new OxyPlot.Annotations.LineAnnotation()
            {
                Type = LineAnnotationType.Vertical,
                X = x,
                Color = color,
                StrokeThickness = 2,
                LineStyle = LineStyle.Solid,
                Layer = AnnotationLayer.BelowSeries,
                TextOrientation = AnnotationTextOrientation.Horizontal,
                TextVerticalAlignment = VerticalAlignment.Top,
                Tag = plan.Id,
                Text = text
            });
        }

        // Horizontal line
        public static OxyPlot.Series.Series CreateHorizontalLine(PlanSetup plan, double value, string yKey, string xKey, double min, double max)
        {
            var series = new OxyPlot.Series.LineSeries();
            series.YAxisKey = yKey;
            series.XAxisKey = xKey;
            series.Tag = plan.Id;

            series.Points.Add(new DataPoint(min, value));
            series.Points.Add(new DataPoint(max, value));

            series.Color = OxyColors.Black;

            return series;
        }

        // Find planes in which structure has contours
        private static List<double> FindPlanesWithStructure(PlanSetup plan, Structure structure)
        {
            List<double> planes = new List<double>();

            if (plan.StructureSet.Structures.Any(s => s.Id == structure.Id))
            {
                for (int z = 0; z < plan.Dose.ZSize; z++)
                {
                    if (structure.GetContoursOnImagePlane(z).Length > 0)
                    {
                        planes.Add(z);
                    }
                }
            }
            return planes;
        }

        // Overload for string input
        private static List<double> FindPlanesWithStructure(PlanSetup plan, string structureID)
        {
            List<double> planes = new List<double>();

            if (plan.StructureSet.Structures.Any(s => s.Id == structureID))
            {
                Structure structure = plan.StructureSet.Structures.First(s => s.Id == structureID);
                for (int z = 0; z < plan.Dose.ZSize; z++)
                {
                    if (structure.GetContoursOnImagePlane(z).Length > 0)
                    {
                        planes.Add(z);
                    }
                }
            }
            return planes;
        }
    }
}
