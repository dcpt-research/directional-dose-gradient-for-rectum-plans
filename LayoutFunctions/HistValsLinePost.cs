﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace RectumDirectionalGradientManyViews.LayoutFunctions
{
    public static class HistValsLinePost
    {
        public static double returnHistValsBladder50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  8.8;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  18.9;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  27.3;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  30.2;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladder75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  10.8;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  22.7;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  33.7;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  39.3;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladderRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  9.9;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  20.4;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  30.0;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  34.7;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  12.2;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  24.2;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  38.6;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  47.1;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  16.8;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  32.2;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  53.9;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  77.7;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowelRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  14.9;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  28.3;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  48.7;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  69.9;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone50(DataPoint dp, double isodoseLevel)
        {

            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  7.9;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  16.9;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  29.6;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  40.1;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  10.3;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  21.1;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  35.2;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  49;
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNoneRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                return  9.5;
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                return  19;
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                return  31.6;
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                return  48.1;
            }
            else
            {
                return  double.NaN;
            }
        }
    }
}
