﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace RectumDirectionalGradientManyViews.LayoutFunctions
{
    public static class HistValsLineHorns
    {
        public static double returnHistValsBladder50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  6.2;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  5.8;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  6.0;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.3;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  6.5;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  6.3;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  7.6;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  7.1;
                }
                else if (dp.Y >= 20 & dp.Y < 45)
                {
                    return  7.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  16.3;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  15.1;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  17.2;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  17.6;
                }
                else if (dp.Y >= 25 & dp.Y < 35)
                {
                    return  18.1;
                }
                else if (dp.Y >= 35 & dp.Y < 45)
                {
                    return  20.6;
                }
                else if (dp.Y >= 45 & dp.Y < 100)
                {
                    return  17.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 15)
                {
                    return  30.8;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  34.1;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  34.3;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  33;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  34;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  38.3;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  43.1;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  41.5;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  42;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  42.6;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  44.9;
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return  45.7;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  35;
                }
                else if (dp.Y > 100 & dp.Y < 150)
                {
                    return  30.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 30)
                {
                    return  48.1;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  59.4;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  57.5;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  58.3;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  63.4;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  68.3;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  66.3;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  67.8;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  89.6;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  73.2;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  81.4;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  84.8;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  66.7;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  71.9;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  72.7;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  77.8;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  75.4;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  73.1;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  77.8;
                }
                else if (dp.Y >= 150 & dp.Y < 180)
                {
                    return  73.5;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladder75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  8.9;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  8.3;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  7.8;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  8.6;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  8.5;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  8.1;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  9.0;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  9.1;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  10;
                }
                else if (dp.Y >= 20 & dp.Y < 45)
                {
                    return  8.8;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  20.6;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  20.4;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  19.8;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  21.4;
                }
                else if (dp.Y >= 25 & dp.Y < 35)
                {
                    return  22.7;
                }
                else if (dp.Y >= 35 & dp.Y < 45)
                {
                    return  22.1;
                }
                else if (dp.Y >= 45 & dp.Y < 100)
                {
                    return  21.5;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 15)
                {
                    return  37.8;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  44.1;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  43.2;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  42.2;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  43.4;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  46.4;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  48.2;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  48.1;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  50.2;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  52.6;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  57.8;
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return  51.6;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  38.4;
                }
                else if (dp.Y > 100 & dp.Y < 150)
                {
                    return  39.4;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 30)
                {
                    return  57.3;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  67.3;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  68.5;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  65.9;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  70.9;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  76.6;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  80.6;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  78.5;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  93.7;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  96.4;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  100.7;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  105.3;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  81.5;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  95.6;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  79.9;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  86.4;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  88.5;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  79.2;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  86.8;
                }
                else if (dp.Y >= 150 & dp.Y < 180)
                {
                    return  102.8;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladderRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  8.6;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  8.1;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  8.2;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  8.6;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7.9;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  8.3;
                }
                else if (dp.Y >= 10 & dp.Y < 40)
                {
                    return  7.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 8)
                {
                    return  20.1;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  19.2;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  20.4;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  19.4;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  17.1;
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return  19.6;
                }
                else if (dp.Y >= 20 & dp.Y < 100)
                {
                    return  19.7;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 12)
                {
                    return  35.3;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  38.4;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  37.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  39.9;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  38.7;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  36.8;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  37.5;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  36.9;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  43.3;
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return  41.2;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 40)
                {
                    return  59.5;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  68.6;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  64.8;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  78.5;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  71;
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return  78;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  91.2;
                }
                else if (dp.Y >= 100 & dp.Y < 110)
                {
                    return  101.9;
                }
                else if (dp.Y >= 110 & dp.Y < 120)
                {
                    return  91.9;
                }
                else if (dp.Y >= 120 & dp.Y < 140)
                {
                    return  85.7;
                }
                else if (dp.Y >= 140 & dp.Y < 160)
                {
                    return  78.3;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  4.2;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  5.2;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  5.5;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.1;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  6.1;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  6.1;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  6.3;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  6.4;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  7;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  6.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  7.2;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  6.8;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  8.1;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  7;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  6.7;
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return  7.3;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return  15.1;
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return  15.1;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  13.7;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  13.8;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  14.8;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  16.2;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  16.6;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  17.6;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  17.4;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  17.8;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  18.3;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  17.4;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  19.6;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  18.3;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  19;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return  35.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  35.7;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  31.7;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  32.8;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  28.8;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  29.9;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  30;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  31.7;
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return  32.4;
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return  34.7;
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return  35.1;
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return  37.8;
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return  39.4;
                }
                else if (dp.Y > 40 & dp.Y < 42)
                {
                    return  39.3;
                }
                else if (dp.Y > 42 & dp.Y < 44)
                {
                    return  40.8;
                }
                else if (dp.Y > 44 & dp.Y < 46)
                {
                    return  39.6;
                }
                else if (dp.Y > 46 & dp.Y < 48)
                {
                    return  39.2;
                }
                else if (dp.Y > 48 & dp.Y < 50)
                {
                    return  39.7;
                }
                else if (dp.Y > 50 & dp.Y < 52)
                {
                    return  44.0;
                }
                else if (dp.Y > 52 & dp.Y < 54)
                {
                    return  37.0;
                }
                else if (dp.Y > 54 & dp.Y < 56)
                {
                    return  38.6;
                }
                else if (dp.Y > 56 & dp.Y < 58)
                {
                    return  40.9;
                }
                else if (dp.Y > 58 & dp.Y < 60)
                {
                    return  43.6;
                }
                else if (dp.Y > 60 & dp.Y < 70)
                {
                    return  47.6;
                }
                else if (dp.Y > 70 & dp.Y < 80)
                {
                    return  52.4;
                }
                else if (dp.Y > 80 & dp.Y < 90)
                {
                    return  51.8;
                }
                else if (dp.Y > 90 & dp.Y < 100)
                {
                    return  46.7;
                }
                else if (dp.Y > 100 & dp.Y < 120)
                {
                    return  34.5;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  49.1;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  52.2;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  56.7;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  61.1;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  66.1;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  67.6;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  70.9;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  75.1;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  78.9;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  82.7;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  84.3;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  86.4;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  89.2;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  81.6;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  79.2;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  82.5;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  79.8;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  83;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  91.7;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  85.1;
                }
                else if (dp.Y >= 150 & dp.Y < 160)
                {
                    return  88.5;
                }
                else if (dp.Y >= 160 & dp.Y < 200)
                {
                    return  85.8;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  7.1;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  7.4;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  6.5;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.9;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7.5;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  7.9;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  8.3;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  8.1;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  8.7;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  8.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  8.9;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  9.0;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  11.7;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  9.1;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  8.5;
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return  8.2;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return  18.3;
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return  20.1;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  20.4;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  18.1;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  17.5;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  17.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  19.2;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  20.8;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  21;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  22.2;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  22.7;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  21.1;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  24;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  21.9;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  23.3;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return  39.6;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  40.4;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  42.4;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  43.1;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  38.2;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  36;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  34.8;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  37.5;
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return  39.3;
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return  37.6;
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return  41.1;
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return  39.2;
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return  40.7;
                }
                else if (dp.Y > 40 & dp.Y < 42)
                {
                    return  42.3;
                }
                else if (dp.Y > 42 & dp.Y < 44)
                {
                    return  44.4;
                }
                else if (dp.Y > 44 & dp.Y < 46)
                {
                    return  45;
                }
                else if (dp.Y > 46 & dp.Y < 48)
                {
                    return  46.3;
                }
                else if (dp.Y > 48 & dp.Y < 50)
                {
                    return  48.3;
                }
                else if (dp.Y > 50 & dp.Y < 52)
                {
                    return  48;
                }
                else if (dp.Y > 52 & dp.Y < 54)
                {
                    return  52.5;
                }
                else if (dp.Y > 54 & dp.Y < 56)
                {
                    return  47.3;
                }
                else if (dp.Y > 56 & dp.Y < 58)
                {
                    return  48.7;
                }
                else if (dp.Y > 58 & dp.Y < 60)
                {
                    return  53.8;
                }
                else if (dp.Y > 60 & dp.Y < 70)
                {
                    return  55.6;
                }
                else if (dp.Y > 70 & dp.Y < 80)
                {
                    return  60.7;
                }
                else if (dp.Y > 80 & dp.Y < 90)
                {
                    return  67.8;
                }
                else if (dp.Y > 90 & dp.Y < 100)
                {
                    return  64.2;
                }
                else if (dp.Y > 100 & dp.Y < 120)
                {
                    return  59.4;
                }
                else if (dp.Y > 120 & dp.Y < 150)
                {
                    return  49.7;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  67.2;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  54.5;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  59.2;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  64.3;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  68.4;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  71.3;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  76;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  80.3;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  87.1;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  90.1;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  92;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  93;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  103.5;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  93.5;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  89.9;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  96.6;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  89.1;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  101.7;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  106.9;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  103.9;
                }
                else if (dp.Y >= 150 & dp.Y < 160)
                {
                    return  108.7;
                }
                else if (dp.Y >= 160 & dp.Y < 200)
                {
                    return  115.4;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowelRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  7.3;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  7.6;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  6.6;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.6;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7.2;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  6.8;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  8.5;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  7.1;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  8.7;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  7.2;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  7.8;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  6.2;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  11.2;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  6.9;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  7.5;
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return  8.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return  18.6;
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return  18.7;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  20.2;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  15.8;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  15.4;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  17.3;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  18.7;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  20.2;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  18.8;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  22.7;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  21.8;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  20.0;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  20.1;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  18.1;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  20.6;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return  37.5;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  40.2;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  44.0;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  37.2;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  36.0;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  36.3;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  28.0;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  37.6;
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return  36.8;
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return  34.1;
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return  36.8;
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return  37.8;
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return  38.8;
                }
                else if (dp.Y >= 40 & dp.Y < 42)
                {
                    return  41.1;
                }
                else if (dp.Y >= 42 & dp.Y < 44)
                {
                    return  43.3;
                }
                else if (dp.Y >= 44 & dp.Y < 46)
                {
                    return  45.0;
                }
                else if (dp.Y >= 46 & dp.Y < 48)
                {
                    return  46.1;
                }
                else if (dp.Y >= 48 & dp.Y < 50)
                {
                    return  49.8;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  52.1;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  49.0;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  49.8;
                }
                else if (dp.Y >= 80 & dp.Y < 100)
                {
                    return  49.5;
                }
                else if (dp.Y >= 100 & dp.Y < 150)
                {
                    return  36.2;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 45)
                {
                    return  62.5;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  64.1;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  60.0;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  59.5;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  62.4;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  66.7;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  70.3;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  75.0;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  76.6;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  81.5;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  87.5;
                }
                else if (dp.Y >= 100 & dp.Y < 110)
                {
                    return  89.9;
                }
                else if (dp.Y >= 110 & dp.Y < 120)
                {
                    return  73.2;
                }
                else if (dp.Y >= 120 & dp.Y < 130)
                {
                    return  83.4;
                }
                else if (dp.Y >= 130 & dp.Y < 140)
                {
                    return  106.5;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  101.9;
                }
                else if (dp.Y >= 150 & dp.Y < 200)
                {
                    return  83.6;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  3.7;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  4.6;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  5.5;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.3;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7.4;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  8.3;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  9.2;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  10.6;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  12.5;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  10.7;
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return  8.6;
                }
                else if (dp.Y >= 20 & dp.Y < 50)
                {
                    return  23.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  9.1;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  11.5;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  13.2;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  15.0;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  16.8;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  18.8;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  20.4;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  22.3;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  24.7;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  27.1;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  31.0;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  40.8;
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return  44.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return  18.9;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  23.4;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  27.5;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  33.1;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  37.2;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  42.2;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  46.8;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  52.2;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  56.9;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  63.2;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  71.0;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  43.3;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  52.7;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  58.2;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  63.2;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  67.2;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  72.7;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  77.1;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  81.8;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  87.8;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  91.6;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  96.6;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  102.2;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  108.2;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  111.0;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  116.1;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  4.6;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  4.9;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  5.8;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.7;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7.8;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  8.7;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  9.7;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  11.4;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  13.3;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  14.6;
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return  16.3;
                }
                else if (dp.Y >= 20 & dp.Y < 50)
                {
                    return  27.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  13.5;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  11.9;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  13.8;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  15.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  17.4;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  19.7;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  21.4;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  23.4;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  25.5;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  28;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  35.1;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  44.8;
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return  50.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return  29.1;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  25.8;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  29.3;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  34.6;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  39.2;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  44.2;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  48.6;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  53.9;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  58.7;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  67;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  75.1;
                }
                else if (dp.Y > 80 & dp.Y < 100)
                {
                    return  84.7;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  48.2;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  54.1;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  59.6;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  64.7;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  69.2;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  74.3;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  79.2;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  83.5;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  89.0;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  93.1;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  98.7;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  103.9;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  109.5;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  112.4;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  117.8;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNoneRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  4.0;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  4.8;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  5.7;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  6.6;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  7.8;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  8.6;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  9.7;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  11.1;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  13.4;
                }
                else if (dp.Y >= 14 & dp.Y < 50)
                {
                    return  16.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  10.2;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  11.8;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  13.6;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  15.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  17.4;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  19.7;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  21.5;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  23.5;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  25.1;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  28.6;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  36.5;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return  18.9;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  25;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  28.8;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  34.5;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  38.5;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  44.3;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  47.4;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  53.2;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  59.1;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  68;
                }
                else if (dp.Y >= 70 & dp.Y < 95)
                {
                    return  77.2;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  45.2;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  54.3;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  59.1;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  64.1;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  69.6;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  73.8;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  78.8;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  84.1;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  89.5;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  93.7;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  99.5;
                }
                else if (dp.Y >= 100 & dp.Y < 120)
                {
                    return  109.4;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }
    }
}
