﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace RectumDirectionalGradientManyViews.LayoutFunctions
{
    public static class HistValsLineLR
    {
        public static double returnHistValsBladder50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return 9.8;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  10.8;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  9.9;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  9.8;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  9.9;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  10.2;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  10.9;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  11.3;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  10.3;
                }
                else if (dp.Y >= 20 & dp.Y < 45)
                {
                    return  10;
                }
                else
                {
                    return double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  23.7;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  24.4;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  25.1;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  30.6;
                }
                else if (dp.Y >= 25 & dp.Y < 35)
                {
                    return  28.2;
                }
                else if (dp.Y >= 35 & dp.Y < 45)
                {
                    return  25.2;
                }
                else if (dp.Y >= 45 & dp.Y < 100)
                {
                    return  21.4;
                }
                else
                {
                    return double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 15)
                {
                    return  61.4;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  61.3;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  60.7;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  64.8;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  70.1;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  63.5;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  82.5;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  76.4;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  68.2;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  76.8;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  84.4;
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return  72.9;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  74.4;
                }
                else if (dp.Y > 100 & dp.Y < 150)
                {
                    return  75.5;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 30)
                {
                    return  108.4;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  108.7;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  114.3;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  121;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  126.7;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  119.1;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  129.4;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  119;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  133.2;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  118.6;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  123.3;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  127.3;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  117.1;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  125.3;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  115.5;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  119;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  123.7;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  126.7;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  125.7;
                }
                else if (dp.Y >= 150 & dp.Y < 180)
                {
                    return  111.7;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladder75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  17.2;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  22.4;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  21.1;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  13.9;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  17.2;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  17.7;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  15.4;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  17.1;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  16.4;
                }
                else if (dp.Y >= 20 & dp.Y < 45)
                {
                    return  13.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  36.1;
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return  39.6;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  36.2;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  42.5;
                }
                else if (dp.Y >= 25 & dp.Y < 35)
                {
                    return  38.3;
                }
                else if (dp.Y >= 35 & dp.Y < 45)
                {
                    return  29.7;
                }
                else if (dp.Y >= 45 & dp.Y < 100)
                {
                    return  25.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 15)
                {
                    return  79.9;
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return  83;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  78.6;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  93.2;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  88.9;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  84.5;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  152.3;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  102.3;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  78;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  93.5;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  115.8;
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return  83.7;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  86.5;
                }
                else if (dp.Y > 100 & dp.Y < 150)
                {
                    return  82.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 30)
                {
                    return  115.3;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  124.1;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  123.4;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  121.2;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  125.9;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  127.7;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  153.3;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  137;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  128.6;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  125.5;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  129.9;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  129.5;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  121.3;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  128.8;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  126.3;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  125.5;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  133.7;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  134.2;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  134.4;
                }
                else if (dp.Y >= 150 & dp.Y < 180)
                {
                    return  123.1;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladderRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  13.1;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  12.1;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  11.2;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  11.8;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  14.6;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  15.8;
                }
                else if (dp.Y >= 10 & dp.Y < 40)
                {
                    return  16.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 8)
                {
                    return  30;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  26.7;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  25;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  24.9;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  26.2;
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return  31.8;
                }
                else if (dp.Y >= 20 & dp.Y < 100)
                {
                    return  37.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 12)
                {
                    return  66.7;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  69.9;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  69.5;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  62.8;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  56;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  62.5;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  69.8;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  66;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  77.6;
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return  73.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 40)
                {
                    return  129.3;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  123.8;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  118.3;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  132.4;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  119.3;
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return  119.7;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  125.8;
                }
                else if (dp.Y >= 100 & dp.Y < 110)
                {
                    return  135;
                }
                else if (dp.Y >= 110 & dp.Y < 120)
                {
                    return  120.9;
                }
                else if (dp.Y >= 120 & dp.Y < 140)
                {
                    return  135.3;
                }
                else if (dp.Y >= 140 & dp.Y < 160)
                {
                    return  132.5;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  7.5;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  8.1;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  9.0;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  8.5;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  8.7;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  8.9;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  8.8;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  10.1;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  9.8;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  8.8;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  10.3;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  9.2;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  11;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  10.5;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  13.1;
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return  9.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return  24.9;
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return  22.3;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  24.1;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  23;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  22.8;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  24.6;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  24.8;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  24.1;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  27.5;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  27.7;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  25.1;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  27.5;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  28.2;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  27.1;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  26.7;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return  60.7;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  64.8;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  77.1;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  66.7;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  61;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  68.1;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  64.8;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  61.3;
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return  61;
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return  60.4;
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return  64.5;
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return  68;
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return  65.5;
                }
                else if (dp.Y > 40 & dp.Y < 42)
                {
                    return  71.3;
                }
                else if (dp.Y > 42 & dp.Y < 44)
                {
                    return  68.5;
                }
                else if (dp.Y > 44 & dp.Y < 46)
                {
                    return  64.2;
                }
                else if (dp.Y > 46 & dp.Y < 48)
                {
                    return  64;
                }
                else if (dp.Y > 48 & dp.Y < 50)
                {
                    return  69.3;
                }
                else if (dp.Y > 50 & dp.Y < 52)
                {
                    return  63.7;
                }
                else if (dp.Y > 52 & dp.Y < 54)
                {
                    return  67;
                }
                else if (dp.Y > 54 & dp.Y < 56)
                {
                    return  66;
                }
                else if (dp.Y > 56 & dp.Y < 58)
                {
                    return  73.8;
                }
                else if (dp.Y > 58 & dp.Y < 60)
                {
                    return  69.4;
                }
                else if (dp.Y > 60 & dp.Y < 70)
                {
                    return  69.5;
                }
                else if (dp.Y > 70 & dp.Y < 80)
                {
                    return  66;
                }
                else if (dp.Y > 80 & dp.Y < 90)
                {
                    return  62.6;
                }
                else if (dp.Y > 90 & dp.Y < 100)
                {
                    return  73;
                }
                else if (dp.Y > 100 & dp.Y < 120)
                {
                    return  74.7;
                }
                else if (dp.Y > 120 & dp.Y < 150)
                {
                    return  74.3;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  101.7;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  98;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  95.3;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  104.3;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  91.8;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  101;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  100.8;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  99.5;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  109.8;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  110.6;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  112.1;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  115.8;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  113.2;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  115.3;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  116.7;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  114.8;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  116.5;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  115.9;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  119;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  121.3;
                }
                else if (dp.Y >= 150 & dp.Y < 160)
                {
                    return  116.9;
                }
                else if (dp.Y >= 160 & dp.Y < 200)
                {
                    return  122.6;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  9.7;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  9.8;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  12.4;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  10.5;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  11.7;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  10.8;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  11.2;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  12.5;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  11.9;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  11.5;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  13.7;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  10.9;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  13.8;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  13.6;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  16.5;
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return  15.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return  29.5;
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return  27.1;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  28.8;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  29.1;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  31;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  32.1;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  31.6;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  29.8;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  35;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  31.6;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  35.7;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  34.1;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  33.7;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  33.9;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  33.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return  83.2;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  84;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  81.8;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  81.5;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  68.6;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  76.5;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  76.4;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  75.1;
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return  73.3;
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return  73.9;
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return  72.9;
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return  80.9;
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return  84.1;
                }
                else if (dp.Y > 40 & dp.Y < 42)
                {
                    return  87.2;
                }
                else if (dp.Y > 42 & dp.Y < 44)
                {
                    return  88.1;
                }
                else if (dp.Y > 44 & dp.Y < 46)
                {
                    return  82.2;
                }
                else if (dp.Y > 46 & dp.Y < 48)
                {
                    return  77.6;
                }
                else if (dp.Y > 48 & dp.Y < 50)
                {
                    return  84.1;
                }
                else if (dp.Y > 50 & dp.Y < 52)
                {
                    return  74.8;
                }
                else if (dp.Y > 52 & dp.Y < 54)
                {
                    return  72.8;
                }
                else if (dp.Y > 54 & dp.Y < 56)
                {
                    return  75.8;
                }
                else if (dp.Y > 56 & dp.Y < 58)
                {
                    return  96.1;
                }
                else if (dp.Y > 58 & dp.Y < 60)
                {
                    return  82;
                }
                else if (dp.Y > 60 & dp.Y < 70)
                {
                    return  86.4;
                }
                else if (dp.Y > 70 & dp.Y < 80)
                {
                    return  77.8;
                }
                else if (dp.Y > 80 & dp.Y < 90)
                {
                    return  74.9;
                }
                else if (dp.Y > 90 & dp.Y < 100)
                {
                    return  87.1;
                }
                else if (dp.Y > 100 & dp.Y < 120)
                {
                    return  83;
                }
                else if (dp.Y > 120 & dp.Y < 150)
                {
                    return  82.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  112;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  106.3;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  106.1;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  112.6;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  107.1;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  108.5;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  111.4;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  109.8;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  118.9;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  117.5;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  118.6;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  122.7;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  126;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  123.1;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  124.4;
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return  125.3;
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return  125;
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return  122.6;
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return  126.3;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  128.4;
                }
                else if (dp.Y >= 150 & dp.Y < 160)
                {
                    return  125.6;
                }
                else if (dp.Y >= 160 & dp.Y < 200)
                {
                    return  126.6;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowelRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  10.7;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  10.4;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  10.1;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  10.5;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  10;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  9.3;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  9.4;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  11.4;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  11.6;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  11.5;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  14.8;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  13.2;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  14.9;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  10.5;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  14.3;
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return  11.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return  24.9;
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return  31.8;
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return  28.8;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  28.7;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  26.8;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  28.1;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  28.5;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  28.8;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  31.6;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  30;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  35.3;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  34.6;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  35.1;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  34.3;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  31.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return  62.4;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  62.9;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  71.5;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  64.8;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  69.0;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  84.7;
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return  70;
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return  59.5;
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return  75.8;
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return  69.3;
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return  68.7;
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return  76.3;
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return  79.2;
                }
                else if (dp.Y >= 40 & dp.Y < 42)
                {
                    return  78.6;
                }
                else if (dp.Y >= 42 & dp.Y < 44)
                {
                    return  87.1;
                }
                else if (dp.Y >= 44 & dp.Y < 46)
                {
                    return  72.6;
                }
                else if (dp.Y >= 46 & dp.Y < 48)
                {
                    return  78.4;
                }
                else if (dp.Y >= 48 & dp.Y < 50)
                {
                    return  92.4;
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return  81;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  89.9;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  72.3;
                }
                else if (dp.Y >= 80 & dp.Y < 100)
                {
                    return  77;
                }
                else if (dp.Y >= 100 & dp.Y < 150)
                {
                    return  80.7;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 45)
                {
                    return  119.7;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  109;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  110;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  108.3;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  111.3;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  106.4;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  107.8;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  119.2;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  114.3;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  113.1;
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return  109.7;
                }
                else if (dp.Y >= 100 & dp.Y < 110)
                {
                    return  125.1;
                }
                else if (dp.Y >= 110 & dp.Y < 120)                {
                    return  120.4;
                }
                else if (dp.Y >= 120 & dp.Y < 130)
                {
                    return  118.5;
                }
                else if (dp.Y >= 130 & dp.Y < 140)
                {
                    return  114.8;
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return  122.8;
                }
                else if (dp.Y >= 150 & dp.Y < 200)
                {
                    return  125.6;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  14.8;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  10.7;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  10.7;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  10.4;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  11.6;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  11.5;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  12.6;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  12.9;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  15.9;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  16.9;
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return  27.1;
                }
                else if (dp.Y >= 20 & dp.Y < 50)
                {
                    return  24.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  32.4;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  23.8;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  23.3;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  24.5;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  25.0;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  23.2;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  25.0;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  25.5;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  25.2;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  32.2;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  35.2;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  49.6;
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return  74.0;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return  62.3;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  52.4;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  48.8;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  51.0;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  54.9;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  49.9;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  55.9;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  56.4;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  58.8;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  61.2;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  61.1;
                }
                else if (dp.Y > 80 & dp.Y < 100)
                {
                    return  89.8;
                }
                else if (dp.Y > 100 & dp.Y < 140)
                {
                    return  87.5;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  106.7;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  100.7;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  118.0;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  123.3;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  122.0;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  122.9;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  123.4;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  121.9;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  125.2;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  130.0;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  129.1;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  148.1;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  133.5;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  136.6;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  141.9;
                }
                else if (dp.Y >= 120 & dp.Y < 150)
                {
                    return  141.8;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  15.1;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  10.7;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  11.9;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  10.7;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  11.6;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  11.5;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  12.6;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  12.9;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  15.9;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  18.3;
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return  30.9;
                }
                else if (dp.Y >= 20 & dp.Y < 50)
                {
                    return  24.8;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  32.4;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  23.8;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  23.3;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  24.5;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  25.4;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  23.6;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  25.0;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  25.5;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  25.2;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  32.2;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  35.2;
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return  58.6;
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return  77.4;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return  62.3;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  53;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  48.8;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  51.0;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  56.1;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  49.9;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  55.9;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  56.4;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  64.8;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  70;
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return  69.2;
                }
                else if (dp.Y > 80 & dp.Y < 100)
                {
                    return  103.9;
                }
                else if (dp.Y > 100 & dp.Y < 140)
                {
                    return  87.5;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  106.7;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  100.7;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  127.6;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  123.3;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  134.3;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  138.7;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  135.7;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  136.1;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  138.2;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  142.5;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  137.9;
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return  155.4;
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return  141.4;
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return  142.5;
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return  148.9;
                }
                else if (dp.Y >= 120 & dp.Y < 150)
                {
                    return  147.6;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNoneRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return  19.0;
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return  12.2;
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return  10.3;
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return  10.5;
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return  10.4;
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return  10.2;
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return  12.7;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  13.5;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  14.1;
                }
                else if (dp.Y >= 14 & dp.Y < 50)
                {
                    return  14.9;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return  35.1;
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return  25.8;
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return  22.7;
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return  23.3;
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return  25;
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return  24;
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return  24.9;
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return  24.3;
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return  28.4;
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return  27.5;
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return  29.3;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return  55.1;
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return  54.8;
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return  49.6;
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return  52.6;
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return  54.1;
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return  47.7;
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return  51.4;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  48.7;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  56.9;
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return  61.1;
                }
                else if (dp.Y >= 70 & dp.Y < 95)
                {
                    return  54.3;
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return  117.9;
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return  127.2;
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return  105.3;
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return  138.7;
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return  126.5;
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return  116.3;
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return  112.0;
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return  124.6;
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return  110.1;
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return  104.2;
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return  124;
                }
                else if (dp.Y >= 100 & dp.Y < 120)
                {
                    return  161.3;
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }
    }
}
