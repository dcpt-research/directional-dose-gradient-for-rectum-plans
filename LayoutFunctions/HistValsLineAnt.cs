﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace RectumDirectionalGradientManyViews.LayoutFunctions
{
    // hard-coded historical values
    // methods return lists of data points to plot dotted lines in the plots
    public static class HistValsAnt
    {
        public static DataPoint returnHistValsBladder50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 4.5));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 5.3));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 6.7));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 6.9));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 10.7));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 13.0));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 15.6));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 15.8));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 21.4));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 9.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 10.7));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 13.5));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 14.6));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 21.8));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 29.6));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 42.9));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 38.3));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 48.2));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 17.4));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 20.4));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 25.9));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 36.7));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 48.4));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 72.9));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 101.4));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 100.7));
                }
                else if (dp.Y > 1.75)
                {
                    return(new DataPoint(dp.X, 87.0));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 77.9));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 90.3));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 111.2));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 123.5));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 133.4));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 137.4));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 144.1));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 131));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 143.7));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsBladder75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 5.7));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 7.4));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 9.0));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 9.7));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 14.2));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 16.1));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 26.2));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 24.1));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 38));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 11.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 14.5));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 16.8));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 20.7));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 34.0));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 41.3));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 51.8));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 42.3));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 55.6));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 21.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 26.1));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 34.9));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 46.9));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 67.6));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 102.8));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 114.1));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 109.3));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 108.7));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 100.2));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 105.7));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 123.7));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 132.4));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 140.4));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.25)
                {
                    return(new DataPoint(dp.X, 145.6));
                }
                else if (dp.Y >= 1.25 & dp.Y < 1.5)
                {
                    return(new DataPoint(dp.X, 156.3));
                }
                else if (dp.Y >= 1.5 & dp.Y < 1.75)
                {
                    return(new DataPoint(dp.X, 138.1));
                }
                else if (dp.Y >= 1.75)
                {
                    return(new DataPoint(dp.X, 151.1));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsBladderRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 4.4));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 5.2));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 7.5));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 8.1));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 7.8));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 24.9));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 9.9));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 10.5));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 14.3));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 16.1));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 16.3));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 32.1));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 18.4));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 17.7));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 25.4));
                }
                else if (dp.Y >= 0.6 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 37.8));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 44.7));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 81.1));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 94.8));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 100.2));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 113.9));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 140.8));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 151.4));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsBowel50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 7.5));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 9.3));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 9.6));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 10.7));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 15.0));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 15.4));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 23.3));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 22.8));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 25.5));
                }
                else if (dp.Y >= 1.8)
                {
                    return(new DataPoint(dp.X, 33.7));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 14.8));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 18.6));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 20.2));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 22.5));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 31.0));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 29.6));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 40.3));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 39.9));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 42.3));
                }
                else if (dp.Y >= 1.8 & dp.Y < 2.0)
                {
                    return(new DataPoint(dp.X, 47.8));
                }
                else if (dp.Y > 2.0)
                {
                    return(new DataPoint(dp.X, 45.4));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 30.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 38.8));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 41.5));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 43.6));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 52.3));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 54.7));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 68.4));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 71.4));
                }
                else if (dp.Y >= 1.6)
                {
                    return(new DataPoint(dp.X, 100.7));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 77.9));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 90.3));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 111.2));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 123.5));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 133.4));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 138.2));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 141.5));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 137.4));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 132.1));
                }
                else if (dp.Y >= 1.8)
                {
                    return(new DataPoint(dp.X, 143.7));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsBowel75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 10));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 12.3));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 15));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 15.2));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 20.3));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 23.5));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 27.6));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 29));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 31.2));
                }
                else if (dp.Y >= 1.8)
                {
                    return(new DataPoint(dp.X, 38));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 20.4));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 23.8));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 29.6));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 29.7));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 38));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 40.5));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 46.5));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 50.0));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 48.1));
                }
                else if (dp.Y >= 1.8 & dp.Y < 2.0)
                {
                    return(new DataPoint(dp.X, 51.5));
                }
                else if (dp.Y > 2.0)
                {
                    return(new DataPoint(dp.X, 55.6));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 43.8));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 51));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 60.5));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 60.1));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 66.1));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 70.9));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 95.2));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 101.4));
                }
                else if (dp.Y >= 1.6 )
                {
                    return(new DataPoint(dp.X, 111.6));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 89.7));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 105));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 117.4));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 128.2));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 140.4));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 153.6));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 148.5));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 153.3));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 150.2));
                }
                else if (dp.Y >= 1.8)
                {
                    return(new DataPoint(dp.X, 152.8));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsBowelRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 6.2));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 8.8));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 11.2));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 11.2));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 19));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 17.3));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 28.8));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 31.2));
                }
                else if (dp.Y >= 1.8 & dp.Y < 2.0)
                {
                    return(new DataPoint(dp.X, 38.4));
                }
                else if (dp.Y >= 2.0)
                {
                    return(new DataPoint(dp.X, 47));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 13.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 16.9));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 20.8));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 22.8));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 32.3));
                }
                else if (dp.Y >= 1.0 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 28.3));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 42.9));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 41.1));
                }
                else if (dp.Y >= 1.6 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 44.2));
                }
                else if (dp.Y >= 1.8 & dp.Y < 2.0)
                {
                    return(new DataPoint(dp.X, 51.3));
                }
                else if (dp.Y >= 2.0)
                {
                    return(new DataPoint(dp.X, 62.3));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 29.7));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 42.8));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 45.6));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 47.6));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 68.8));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 73));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.6)
                {
                    return(new DataPoint(dp.X, 74));
                }
                else if (dp.Y >= 1.6)
                {
                    return(new DataPoint(dp.X, 98.9));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 78.2));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 95.4));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 100.2));
                }
                else if (dp.Y >= 0.6 & dp.Y < 0.8)
                {
                    return(new DataPoint(dp.X, 112.9));
                }
                else if (dp.Y >= 0.8 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 135.1));
                }
                else if (dp.Y >= 1.2 & dp.Y < 1.4)
                {
                    return(new DataPoint(dp.X, 142.3));
                }
                else if (dp.Y >= 1.4 & dp.Y < 1.8)
                {
                    return(new DataPoint(dp.X, 151.5));
                }
                else if (dp.Y >= 1.8)
                {
                    return(new DataPoint(dp.X, 157.0));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsNone50(DataPoint dp, double isodoseLevel)
        {
            List<DataPoint> retList = new List<DataPoint>(); // initialize list for return values

            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 7.7));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 8.9));
                }
                else if (dp.Y >= 0.4 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 14.4));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 15.9));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 22.2));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 31.3));
                }
                else if (dp.Y >= 0.6 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 32.3));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 33.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 46.6));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 60.7));
                }
                else if (dp.Y >= 0.6 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 63.9));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 99.1));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 96));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 97.9));
                }
                else if (dp.Y >= 0.6 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 120.2));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsNone75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 9.3));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 15.2));
                }
                else if (dp.Y >= 0.4 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 17));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 20.5));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 30.2));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 39.3));
                }
                else if (dp.Y >= 0.6 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 40.8));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.2)
                {
                    return(new DataPoint(dp.X, 44));
                }
                else if (dp.Y >= 0.2 & dp.Y < 0.4)
                {
                    return(new DataPoint(dp.X, 54.5));
                }
                else if (dp.Y >= 0.4 & dp.Y < 0.6)
                {
                    return(new DataPoint(dp.X, 70.1));
                }
                else if (dp.Y >= 0.6 & dp.Y < 1.2)
                {
                    return(new DataPoint(dp.X, 76.3));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
               if (dp.Y < 0.2)
               {
                   return(new DataPoint(dp.X, 111.4));
               }
               else if (dp.Y >= 0.2 & dp.Y < 0.4)
               {
                   return(new DataPoint(dp.X, 125.8));
               }
               else if (dp.Y >= 0.4 & dp.Y < 0.6)
               {
                   return(new DataPoint(dp.X, 119.4));
               }
               else if (dp.Y >= 0.6 & dp.Y < 1.2)
               {
                   return(new DataPoint(dp.X, 120.9));
               }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }

        public static DataPoint returnHistValsNoneRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 0.5)
                {
                    return(new DataPoint(dp.X, 9.7));
                }
                else if (dp.Y >= 0.5 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 10.4));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 0.5)
                {
                    return(new DataPoint(dp.X, 20.7));
                }
                else if (dp.Y >= 0.5 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 24));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 0.5)
                {
                    return(new DataPoint(dp.X, 67.9));
                }
                else if (dp.Y >= 0.5 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 94.2));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 0.5)
                {
                    return(new DataPoint(dp.X, 101.3));
                }
                else if (dp.Y >= 0.5 & dp.Y < 1.0)
                {
                    return(new DataPoint(dp.X, 138.8));
                }
                else
                {
                    return new DataPoint(dp.X, double.NaN);
                }
            }
            else
            {
                return new DataPoint(dp.X, double.NaN);
            }
        }
    }
}
