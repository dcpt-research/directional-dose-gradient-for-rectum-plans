﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace RectumDirectionalGradientManyViews.LayoutFunctions
{
    public static class HistValsLineLowLR
    {
        public static double returnHistValsBladder50(DataPoint dp, double isodoseLevel)
        {
            if(Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 9.1);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 9.7);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 8.9);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 8.9);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 8.1);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 8.7);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 10.2);
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return ( 9.8);
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return ( 11.1);
                }
                else if (dp.Y >= 20 & dp.Y < 45)
                {
                    return ( 12.3);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return ( 25.6);
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return ( 24.4);
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return ( 24.7);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 25.9);
                }
                else if (dp.Y >= 25 & dp.Y < 35)
                {
                    return ( 28.2);
                }
                else if (dp.Y >= 35 & dp.Y < 45)
                {
                    return ( 28.9);
                }
                else if (dp.Y >= 45 & dp.Y < 100)
                {
                    return ( 27.2);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 15)
                {
                    return ( 65.5);
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return ( 59.7);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 59.8);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 60.2);
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return ( 60.7);
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return ( 61);
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return ( 60.9);
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return ( 63.8);
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return ( 67.5);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 68.9);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 75.6);
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return ( 70.8);
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return ( 78);
                }
                else if (dp.Y > 100 & dp.Y < 150)
                {
                    return ( 73.4);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 30)
                {
                    return ( 110.5);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 109.3);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 110.8);
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return ( 118.8);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 122.6);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 116.6);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 131.7);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 122.8);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 123.8);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 119.8);
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return ( 125.8);
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return ( 129.9);
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return ( 117.6);
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return ( 124.9);
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return ( 120.4);
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return ( 121.5);
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return ( 127.9);
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return ( 126.1);
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return ( 125.6);
                }
                else if (dp.Y >= 150 & dp.Y < 180)
                {
                    return ( 130.7);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladder75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 11.5);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 11.8);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 11.3);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 11.7);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 10.9);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 12);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 15.1);
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return ( 13.3);
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return ( 14.6);
                }
                else if (dp.Y >= 20 & dp.Y < 45)
                {
                    return ( 15.7);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return ( 30.4);
                }
                else if (dp.Y >= 10 & dp.Y < 15)
                {
                    return ( 28.9);
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return ( 30.9);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 31.6);
                }
                else if (dp.Y >= 25 & dp.Y < 35)
                {
                    return ( 32.8);
                }
                else if (dp.Y >= 35 & dp.Y < 45)
                {
                    return ( 35.1);
                }
                else if (dp.Y >= 45 & dp.Y < 100)
                {
                    return ( 33.3);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 15)
                {
                    return ( 76.6);
                }
                else if (dp.Y >= 15 & dp.Y < 20)
                {
                    return ( 69.9);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 72.6);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 72.6);
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return ( 73.8);
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return ( 79.4);
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return ( 73.1);
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return ( 73.8);
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return ( 81.2);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 84.5);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 85.7);
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return ( 88.2);
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return ( 88.4);
                }
                else if (dp.Y > 100 & dp.Y < 150)
                {
                    return ( 89.3);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 30)
                {
                    return ( 113.7);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 118);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 118.7);
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return ( 131);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 142.2);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 137);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 145.1);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 138.3);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 141.9);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 128.1);
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return ( 136.6);
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return ( 139.6);
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return ( 131.8);
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return ( 138.6);
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return ( 135.2);
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return ( 135.7);
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return ( 138.3);
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return ( 130.2);
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return ( 145.5);
                }
                else if (dp.Y >= 150 & dp.Y < 180)
                {
                    return ( 139.6);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBladderRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 12.7);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 12.9);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 10.2);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 12.2);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 11.2);
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return ( 13.8);
                }
                else if (dp.Y >= 10 & dp.Y < 40)
                {
                    return ( 16.2);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 8)
                {
                    return ( 30.4);
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return ( 29.9);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 27.2);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 35.2);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 39.1);
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return ( 38.9);
                }
                else if (dp.Y >= 20 & dp.Y < 100)
                {
                    return ( 40.5);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 12)
                {
                    return ( 84.8);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 69.1);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 70.2);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 69.7);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 67.2);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 72.7);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 74.3);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 90.4);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 91.2);
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return ( 103.3);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 40)
                {
                    return ( 140.4);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 118.9);
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return ( 127.2);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 123.8);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 122.1);
                }
                else if (dp.Y >= 80 & dp.Y < 90)
                {
                    return ( 125);
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return ( 153.1);
                }
                else if (dp.Y >= 100 & dp.Y < 110)
                {
                    return ( 136);
                }
                else if (dp.Y >= 110 & dp.Y < 120)
                {
                    return ( 132.8);
                }
                else if (dp.Y >= 120 & dp.Y < 140)
                {
                    return ( 144);
                }
                else if (dp.Y >= 140 & dp.Y < 160)
                {
                    return ( 140.7);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 11.8);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 12.6);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 13);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 12);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 14.2);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 13.3);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 14.3);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 13.8);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 12.7);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 14);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 14.4);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 14.4);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 14.6);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 13.4);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 13);
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return ( 11.3);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return ( 27);
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return ( 27.3);
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return ( 27.4);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 30.2);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 30.5);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 32.7);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 33.2);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 29.8);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 29.1);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 31.3);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 32.1);
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return ( 30.2);
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return ( 29);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 32.8);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 30.4);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return ( 63);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 63.5);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 70.5);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 68.2);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 69.4);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 69.4);
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return ( 73.7);
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return ( 72.2);
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return ( 71.8);
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return ( 71.9);
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return ( 67.9);
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return ( 71.5);
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return ( 68.8);
                }
                else if (dp.Y > 40 & dp.Y < 42)
                {
                    return ( 79.2);
                }
                else if (dp.Y > 42 & dp.Y < 44)
                {
                    return ( 72.4);
                }
                else if (dp.Y > 44 & dp.Y < 46)
                {
                    return ( 69.9);
                }
                else if (dp.Y > 46 & dp.Y < 48)
                {
                    return ( 66);
                }
                else if (dp.Y > 48 & dp.Y < 50)
                {
                    return ( 69);
                }
                else if (dp.Y > 50 & dp.Y < 52)
                {
                    return ( 67);
                }
                else if (dp.Y > 52 & dp.Y < 54)
                {
                    return ( 66.7);
                }
                else if (dp.Y > 54 & dp.Y < 56)
                {
                    return ( 64.5);
                }
                else if (dp.Y > 56 & dp.Y < 58)
                {
                    return ( 73.8);
                }
                else if (dp.Y > 58 & dp.Y < 60)
                {
                    return ( 72.3);
                }
                else if (dp.Y > 60 & dp.Y < 70)
                {
                    return ( 73);
                }
                else if (dp.Y > 70 & dp.Y < 80)
                {
                    return ( 75);
                }
                else if (dp.Y > 80 & dp.Y < 90)
                {
                    return ( 82.2);
                }
                else if (dp.Y > 90 & dp.Y < 100)
                {
                    return ( 77.7);
                }
                else if (dp.Y > 100 & dp.Y < 120)
                {
                    return ( 74.1);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return ( 113.4);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 123.5);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 118.8);
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return ( 118.9);
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return ( 120.7);
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return ( 117.3);
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return ( 119.9);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 125.3);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 133.2);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 133.4);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 127.6);
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return ( 137.2);
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return ( 134);
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return ( 126.8);
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return ( 128.8);
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return ( 128.5);
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return ( 130.2);
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return ( 129.8);
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return ( 133.4);
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return ( 140.5);
                }
                else if (dp.Y >= 150 & dp.Y < 160)
                {
                    return ( 137.5);
                }
                else if (dp.Y >= 160 & dp.Y < 200)
                {
                    return ( 141.7);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsBowel75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 12.2);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 14.4);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 13);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 12);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 14.4);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 13.4);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 15.2);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 14.9);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 12.8);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 15.2);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 16.5);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 16.2);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 17.6);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 17.3);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 17.7);
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return ( 11.9);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return ( 29.5);
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return ( 32.2);
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return ( 29.3);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 32.2);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 32.3);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 34.7);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 34.3);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 30.1);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 31.6);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 35.3);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 34.4);
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return ( 33.9);
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return ( 32.1);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 37.8);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 36);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return ( 72.5);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 74.6);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 80.5);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 82.7);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 83.5);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 73.9);
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return ( 76.6);
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return ( 72.2);
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return ( 72.3);
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return ( 71.9);
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return ( 69.7);
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return ( 79.4);
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return ( 73.4);
                }
                else if (dp.Y > 40 & dp.Y < 42)
                {
                    return ( 80.1);
                }
                else if (dp.Y > 42 & dp.Y < 44)
                {
                    return ( 75.6);
                }
                else if (dp.Y > 44 & dp.Y < 46)
                {
                    return ( 73.6);
                }
                else if (dp.Y > 46 & dp.Y < 48)
                {
                    return ( 66.4);
                }
                else if (dp.Y > 48 & dp.Y < 50)
                {
                    return ( 70.7);
                }
                else if (dp.Y > 50 & dp.Y < 52)
                {
                    return ( 71.8);
                }
                else if (dp.Y > 52 & dp.Y < 54)
                {
                    return ( 68.6);
                }
                else if (dp.Y > 54 & dp.Y < 56)
                {
                    return ( 68.2);
                }
                else if (dp.Y > 56 & dp.Y < 58)
                {
                    return ( 81.1);
                }
                else if (dp.Y > 58 & dp.Y < 60)
                {
                    return ( 79.8);
                }
                else if (dp.Y > 60 & dp.Y < 70)
                {
                    return ( 79.3);
                }
                else if (dp.Y > 70 & dp.Y < 80)
                {
                    return ( 78.3);
                }
                else if (dp.Y > 80 & dp.Y < 90)
                {
                    return ( 81.7);
                }
                else if (dp.Y > 90 & dp.Y < 100)
                {
                    return ( 89.5);
                }
                else if (dp.Y > 100 & dp.Y < 120)
                {
                    return ( 83.1);
                }
                else if (dp.Y > 120 & dp.Y < 150)
                {
                    return ( 84.8);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return ( 121);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 126.5);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 118.8);
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return ( 118.9);
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return ( 125.2);
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return ( 117.3);
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return ( 123.2);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 129.2);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 133.6);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 138.7);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 135.5);
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return ( 145.9);
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return ( 138.6);
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return ( 136.4);
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return ( 141.7);
                }
                else if (dp.Y >= 120 & dp.Y < 125)
                {
                    return ( 140.6);
                }
                else if (dp.Y >= 125 & dp.Y < 130)
                {
                    return ( 147.5);
                }
                else if (dp.Y >= 130 & dp.Y < 135)
                {
                    return ( 141);
                }
                else if (dp.Y >= 135 & dp.Y < 140)
                {
                    return ( 150);
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return ( 152.8);
                }
                else if (dp.Y >= 150 & dp.Y < 160)
                {
                    return ( 147.7);
                }
                else if (dp.Y >= 160 & dp.Y < 200)
                {
                    return ( 149.6);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }


        public static double returnHistValsBowelRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 13.3);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 15.2);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 10.5);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 12.6);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 14.8);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 12.9);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 15);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 17.4);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 18.2);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 14.3);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 17);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 15.9);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 18.7);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 19.8);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 25.6);
                }
                else if (dp.Y >= 40 & dp.Y < 60)
                {
                    return ( 10.7);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 6)
                {
                    return ( 29.8);
                }
                else if (dp.Y >= 6 & dp.Y < 8)
                {
                    return ( 32.2);
                }
                else if (dp.Y >= 8 & dp.Y < 10)
                {
                    return ( 28);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 32.2);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 31.8);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 34.9);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 33);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 32.8);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 36);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 36.4);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 34.2);
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return ( 39.7);
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return ( 39.9);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 42.5);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 49.6);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 16)
                {
                    return ( 69.9);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 63);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 71.2);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 73.4);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 85.3);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 74.2);
                }
                else if (dp.Y >= 26 & dp.Y < 28)
                {
                    return ( 79.1);
                }
                else if (dp.Y >= 28 & dp.Y < 30)
                {
                    return ( 60);
                }
                else if (dp.Y >= 30 & dp.Y < 32)
                {
                    return ( 77.1);
                }
                else if (dp.Y >= 32 & dp.Y < 34)
                {
                    return ( 70.6);
                }
                else if (dp.Y >= 34 & dp.Y < 36)
                {
                    return ( 70.4);
                }
                else if (dp.Y >= 36 & dp.Y < 38)
                {
                    return ( 81.4);
                }
                else if (dp.Y >= 38 & dp.Y < 40)
                {
                    return ( 75.7);
                }
                else if (dp.Y >= 40 & dp.Y < 42)
                {
                    return ( 78.4);
                }
                else if (dp.Y >= 42 & dp.Y < 44)
                {
                    return ( 98.4);
                }
                else if (dp.Y >= 44 & dp.Y < 46)
                {
                    return ( 76.9);
                }
                else if (dp.Y >= 46 & dp.Y < 48)
                {
                    return ( 74.5);
                }
                else if (dp.Y >= 48 & dp.Y < 50)
                {
                    return ( 78.5);
                }
                else if (dp.Y >= 50 & dp.Y < 60)
                {
                    return ( 73);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 78.8);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 79.4);
                }
                else if (dp.Y >= 80 & dp.Y < 100)
                {
                    return ( 92.6);
                }
                else if (dp.Y >= 100 & dp.Y < 150)
                {
                    return ( 101.9);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 45)
                {
                    return ( 113.1);
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return ( 113.4);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 123.3);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 118.8);
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return ( 113.1);
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return ( 125.6);
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return ( 128.5);
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return ( 124);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 132.8);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 142.2);
                }
                else if (dp.Y >= 90 & dp.Y < 100)
                {
                    return ( 140.9);
                }
                else if (dp.Y >= 100 & dp.Y < 110)
                {
                    return ( 157.1);
                }
                else if (dp.Y >= 110 & dp.Y < 120)
                {
                    return ( 145.7);
                }
                else if (dp.Y >= 120 & dp.Y < 130)
                {
                    return ( 140.6);
                }
                else if (dp.Y >= 130 & dp.Y < 140)
                {
                    return ( 140.5);
                }
                else if (dp.Y >= 140 & dp.Y < 150)
                {
                    return ( 155.4);
                }
                else if (dp.Y >= 150 & dp.Y < 200)
                {
                    return ( 146.9);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone50(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 7.8);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 8.5);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 8.8);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 8.3);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 8.9);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 8.7);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 9.1);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 9.2);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 9.7);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 10.6);
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return ( 10.6);
                }
                else if (dp.Y >= 20 & dp.Y < 50)
                {
                    return ( 8.9);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return ( 22.0);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 21.6);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 21.3);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 20.1);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 19.7);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 20.5);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 20.6);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 21.9);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 23.5);
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return ( 22.5);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 23.1);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 26.3);
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return ( 24);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return ( 50.7);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 50);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 45.5);
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return ( 46.7);
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return ( 47.4);
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return ( 47.0);
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return ( 47.4);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 49.5);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 52.1);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 55.1);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 52.9);
                }
                else if (dp.Y > 80 & dp.Y < 100)
                {
                    return ( 69.9);
                }
                else if (dp.Y > 100 & dp.Y < 140)
                {
                    return ( 118.7);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return ( 94);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 96.2);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 104.7);
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return ( 98.6);
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return ( 102.6);
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return ( 112.3);
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return ( 114.2);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 109.8);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 122.5);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 117.8);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 122.2);
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return ( 126.1);
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return ( 128);
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return ( 133.6);
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return ( 129.2);
                }
                else if (dp.Y >= 120 & dp.Y < 150)
                {
                    return ( 137.0);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNone75(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 9.4);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 10.9);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 10.2);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 10);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 10.3);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 11.0);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 10.7);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 10.7);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 12.4);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 13.8);
                }
                else if (dp.Y >= 16 & dp.Y < 20)
                {
                    return ( 13.8);
                }
                else if (dp.Y >= 20 & dp.Y < 50)
                {
                    return ( 12.3);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return ( 26.8);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 23.5);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 23.5);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 22.8);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 22.4);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 24.0);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 24.5);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 25.2);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 26.2);
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return ( 27.4);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 29.0);
                }
                else if (dp.Y >= 40 & dp.Y < 50)
                {
                    return ( 33.2);
                }
                else if (dp.Y >= 50 & dp.Y < 100)
                {
                    return ( 29.9);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return ( 59.0);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 57.7);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 50.6);
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return ( 51.6);
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return ( 54.5);
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return ( 53.7);
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return ( 55.6);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 61.4);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 62.7);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 80.5);
                }
                else if (dp.Y >= 70 & dp.Y < 80)
                {
                    return ( 66.9);
                }
                else if (dp.Y > 80 & dp.Y < 100)
                {
                    return ( 96.2);
                }
                else if (dp.Y > 100 & dp.Y < 140)
                {
                    return ( 127.0);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return ( 109.4);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 120);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 122.5);
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return ( 115.8);
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return ( 125.2);
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return ( 1134.2);
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return ( 130.0);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 128.6);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 133.2);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 129.2);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 132.6);
                }
                else if (dp.Y >= 100 & dp.Y < 105)
                {
                    return ( 131.0);
                }
                else if (dp.Y >= 105 & dp.Y < 110)
                {
                    return ( 139.7);
                }
                else if (dp.Y >= 110 & dp.Y < 115)
                {
                    return ( 141.8);
                }
                else if (dp.Y >= 115 & dp.Y < 120)
                {
                    return ( 150.6);
                }
                else if (dp.Y >= 120 & dp.Y < 150)
                {
                    return ( 146.5);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

        public static double returnHistValsNoneRP(DataPoint dp, double isodoseLevel)
        {
            if (Math.Abs(isodoseLevel - 40) < 0.01) // 40 Gy
            {
                if (dp.Y < 4)
                {
                    return ( 9.4);
                }
                else if (dp.Y >= 4 & dp.Y < 5)
                {
                    return ( 11.6);
                }
                else if (dp.Y >= 5 & dp.Y < 6)
                {
                    return ( 10.9);
                }
                else if (dp.Y >= 6 & dp.Y < 7)
                {
                    return ( 12.7);
                }
                else if (dp.Y >= 7 & dp.Y < 8)
                {
                    return ( 11.4);
                }
                else if (dp.Y >= 8 & dp.Y < 9)
                {
                    return ( 13.9);
                }
                else if (dp.Y >= 9 & dp.Y < 10)
                {
                    return ( 12.1);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 9.1);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 11.1);
                }
                else if (dp.Y >= 14 & dp.Y < 50)
                {
                    return ( 16.7);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 30) < 0.01) // 30 Gy
            {
                if (dp.Y < 10)
                {
                    return ( 27);
                }
                else if (dp.Y >= 10 & dp.Y < 12)
                {
                    return ( 22.6);
                }
                else if (dp.Y >= 12 & dp.Y < 14)
                {
                    return ( 27.5);
                }
                else if (dp.Y >= 14 & dp.Y < 16)
                {
                    return ( 27.0);
                }
                else if (dp.Y >= 16 & dp.Y < 18)
                {
                    return ( 23.5);
                }
                else if (dp.Y >= 18 & dp.Y < 20)
                {
                    return ( 26.6);
                }
                else if (dp.Y >= 20 & dp.Y < 22)
                {
                    return ( 27.4);
                }
                else if (dp.Y >= 22 & dp.Y < 24)
                {
                    return ( 29.4);
                }
                else if (dp.Y >= 24 & dp.Y < 26)
                {
                    return ( 29.9);
                }
                else if (dp.Y >= 26 & dp.Y < 30)
                {
                    return ( 27.3);
                }
                else if (dp.Y >= 30 & dp.Y < 40)
                {
                    return ( 21.0);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 20) < 0.01) // 20 Gy
            {
                if (dp.Y < 20)
                {
                    return ( 56.7);
                }
                else if (dp.Y >= 20 & dp.Y < 25)
                {
                    return ( 62.1);
                }
                else if (dp.Y >= 25 & dp.Y < 30)
                {
                    return ( 61.5);
                }
                else if (dp.Y >= 30 & dp.Y < 35)
                {
                    return ( 52.9);
                }
                else if (dp.Y >= 35 & dp.Y < 40)
                {
                    return ( 54);
                }
                else if (dp.Y >= 40 & dp.Y < 45)
                {
                    return ( 60.2);
                }
                else if (dp.Y >= 45 & dp.Y < 50)
                {
                    return ( 56);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 59.3);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 63.7);
                }
                else if (dp.Y >= 60 & dp.Y < 70)
                {
                    return ( 95.9);
                }
                else if (dp.Y >= 70 & dp.Y < 95)
                {
                    return ( 62.2);
                }
                else
                {
                    return  double.NaN;
                }
            }

            else if (Math.Abs(isodoseLevel - 10) < 0.01) // 10 Gy
            {
                if (dp.Y < 50)
                {
                    return ( 124.8);
                }
                else if (dp.Y >= 50 & dp.Y < 55)
                {
                    return ( 140.3);
                }
                else if (dp.Y >= 55 & dp.Y < 60)
                {
                    return ( 137.9);
                }
                else if (dp.Y >= 60 & dp.Y < 65)
                {
                    return ( 137.2);
                }
                else if (dp.Y >= 65 & dp.Y < 70)
                {
                    return ( 130.6);
                }
                else if (dp.Y >= 70 & dp.Y < 75)
                {
                    return ( 121.6);
                }
                else if (dp.Y >= 75 & dp.Y < 80)
                {
                    return ( 113);
                }
                else if (dp.Y >= 80 & dp.Y < 85)
                {
                    return ( 120.6);
                }
                else if (dp.Y >= 85 & dp.Y < 90)
                {
                    return ( 130.7);
                }
                else if (dp.Y >= 90 & dp.Y < 95)
                {
                    return ( 127.9);
                }
                else if (dp.Y >= 95 & dp.Y < 100)
                {
                    return ( 125.9);
                }
                else if (dp.Y >= 100 & dp.Y < 120)
                {
                    return ( 133.7);
                }
                else
                {
                    return  double.NaN;
                }
            }
            else
            {
                return  double.NaN;
            }
        }

    }
}
