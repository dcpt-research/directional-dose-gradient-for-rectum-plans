﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace RectumDirectionalGradientManyViews.ViewModels
{
    // The plot at the top of the page showing the PTV's form

    public class ConcavenessViewModel
    {
        // public fields
        public PlotModel ConcavenessPlot { get; private set; }
        private PlanSetup _plan;

        //Constructor
        public ConcavenessViewModel(ScriptContext context)
        {
            ConcavenessPlot = new PlotModel();
            _plan = context.PlanSetup;

            setAxes(ConcavenessPlot);
            ConcavenessPlot.Title = "Concaveness Index";
            ConcavenessPlot.IsLegendVisible = false;
        }

        #region Axes etc
        private void setAxes(PlotModel pm)
        {

            // x-axis
            var xaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "CC position [cm]",
                TitleFontSize = 14,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 10,
                FontSize = 12,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                Key = "CC position [cm]"
            };

            // y-axis
            var yaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "CI",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 10,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                Key = "ConcavenessIndex"
            };

            pm.Axes.Add(xaxis);
            pm.Axes.Add(yaxis);
        }


        #endregion

        #region export logic
        public void ExportPlotAsPdf(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                OxyPlot.Pdf.PdfExporter.Export(ConcavenessPlot, stream, 600, 400);
            }
        }



        // Export plotted data as txt file
        public void ExportDataAsTxt(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in ConcavenessPlot.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.AreaSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LineSeries().GetType()))
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        sw.WriteLine(string.Format("#{0}\t{1}", ConcavenessPlot.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Bottom).Title, ConcavenessPlot.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Left).Title));
                        var dataSer = (OxyPlot.Series.DataPointSeries)ser;
                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X.ToString(), point.Y.ToString()));
                        }
                        sw.WriteLine(string.Format("\n\n"));
                    }
                }
            }
        }
        #endregion
    }
}

