﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using MoreLinq;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;


namespace RectumDirectionalGradientManyViews.ViewModels
{
    public class MainViewModel
    {
        #region public fields
        // view models to pass to xaml
        public FirstViewModel firstViewModel { get; private set; }
        public SecondViewModel secondViewModel { get; private set; }
        public ThirdViewModel thirdViewModel { get; private set; }
        public FourthViewModel fourthViewModel { get; private set; }
        public ConcavenessViewModel concavenessViewModel { get; private set; }
        //public ConcavenessViewModel concavenessViewModel2 { get; private set; }
        public BodyDistanceViewModel bodyDistViewModel { get; private set; }

        // Structures to pass to xaml
        public IEnumerable<Structure> Structures { get; private set; }

        public string ChosenStructureId { get; set; }
        public string patientID { get; private set; }

        // evaluation dose level
        public double doseInput { get; set; }

        // set search lines
        public double lineXInput { get; set; }
        public double lineYInput { get; set; }
        public double lowYInput { get; set; }

        private double lineX { get; set; }
        private double lineY { get; set; }
        private double lowY { get; set; }

        // search grid resolution
        public double spatialResInput { get; set; }
        public double doseResInput { get; set; }


        private readonly PlanSetup _plan;
        private readonly Image image;

        // save target concaveness and anterior gradient to a variable so they can be reused in plots without recalculating
        private List<DataPoint> Concaveness;
        private List<DataPoint> Anterior;

        // I'm using the FindStructureSlices function a lot to re-calculate the same thing. Better to save Bladder, Bowel, and oConcave slices as variables
        private List<double> BladderPlanes;
        private List<double> BowelPlanes;
        private List<double> oConcPlanes;
        private List<double> strPlanes;

        #endregion

        #region Constructor
        public MainViewModel(ScriptContext context)
        {
            Structures = GetPlanStructures(context); // to populate the combo box
            _plan = context.PlanSetup;
            _plan.DoseValuePresentation = DoseValuePresentation.Absolute; // we want only absolute doses!
            patientID = context.Patient.Id;
            image = context.Image;
            Concaveness = new List<DataPoint>();

            //Else set to 0
            try
            {
                lineX = _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.x;
            }
            catch
            {
                lineX = 0;
            }


            // Set default LR line Y-values to mean of the femoral head center points and 5cm below, respectively.
            lineY = FindFemoralHeadMidY(_plan);
            lowY = lineY + 50; // distances are all in mm

            // set public input position values. These are taken with respect to the isocenter
            lineXInput = lineX - _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.x;
            lineYInput = lineY - _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.y;
            lowYInput = lowY - _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.y;

            // populate the four plotviews
            // I tried doing this with four instances of a single viewModel class. That didn't work. Don't know why. This is less elegant but it works, so I kept it this way for now.
            
            // Plots on top to show PTV concaveness. Here it works with just one class??? Maybe because I want them to show the same thing...
            concavenessViewModel = new ConcavenessViewModel(context);
            //concavenessViewModel2 = new ConcavenessViewModel(context);
            bodyDistViewModel = new BodyDistanceViewModel(context);

            firstViewModel = new FirstViewModel(context);
            secondViewModel = new SecondViewModel(context);
            thirdViewModel = new ThirdViewModel(context);
            fourthViewModel = new FourthViewModel(context);
        }
        #endregion

        #region private methods to set properties
        // Get a list of all structures in the currently loaded structure set
        private IEnumerable<Structure> GetPlanStructures(ScriptContext context)
        {
            var plan = context.PlanSetup != null ? context.PlanSetup : null;
            return plan.StructureSet != null
                ? plan.StructureSet.Structures : null;
        }

        // given an ID, find the structure object
        private Structure GetStructure(string id)
        {
            if (Structures.Any(s => s.Id.ToUpper() == id.ToUpper()))
                return Structures.First(s => s.Id.ToUpper() == id.ToUpper());
            else
            {
                System.Windows.MessageBox.Show("Could not find saved structure! Showing first structure in structure set.");
                return Structures.First();
            }
        }
        #endregion

        #region public methods to manipulate the plots

        // Clear previous plot model
        public void ClearAll()
        {
            RemoveAllSeries(firstViewModel.PlotModelOne);
            RemoveAllSeries(secondViewModel.PlotModelTwo);
            RemoveAllSeries(thirdViewModel.PlotModelThree);
            RemoveAllSeries(fourthViewModel.PlotModelFour);
            RemoveAllSeries(concavenessViewModel.ConcavenessPlot);
            //RemoveAllSeries(concavenessViewModel2.ConcavenessPlot);
            RemoveAllSeries(bodyDistViewModel.BodyDistPlot);
        }

        // Clear previous plot model
        public void ClearGradients()
        {
            RemoveAllSeries(firstViewModel.PlotModelOne);
            RemoveAllSeries(secondViewModel.PlotModelTwo);
            RemoveAllSeries(thirdViewModel.PlotModelThree);
            RemoveAllSeries(fourthViewModel.PlotModelFour);
        }

        // Clear all plot series when input is changed
        public void RemoveAllSeries(PlotModel pm)
        {
            pm.Series.Clear();
            pm.Annotations.Clear();
            pm.InvalidatePlot(true);
        }

        public void ShowPlots()
        {
            // if no dose is defined
            if (_plan.Dose == null)
            {
                System.Windows.MessageBox.Show("Plan has no dose!");
                return;
            }

            // Find structure planes to define min and max for y=0 line and x-axis
            if (strPlanes == null)
            {
                strPlanes = FindPlanesWithStructure(_plan, GetStructure(ChosenStructureId));
            }
            if (BladderPlanes == null & _plan.StructureSet.Structures.Any(s => s.Id.ToUpper() == "Bladder".ToUpper()))
            {
                BladderPlanes = FindPlanesWithStructure(_plan, GetStructure("Bladder"));
            }
            if (BowelPlanes == null & _plan.StructureSet.Structures.Any(s => s.Id.ToUpper() == "Bowel_cavity".ToUpper()))
            {
                BowelPlanes = FindPlanesWithStructure(_plan, GetStructure("Bowel_cavity"));
            }
            if (oConcPlanes == null & _plan.StructureSet.Structures.Any(s => s.Id.ToUpper() == "o PTV concav".ToUpper()))
            {
                oConcPlanes = FindPlanesWithStructure(_plan, GetStructure("o PTV concav"));
            }

            // Try to save some time by not recalculating this each time
            if (concavenessViewModel.ConcavenessPlot.Series.Count() == 0)
            {
                // top plot, showing PTV's concaveness
                Concaveness = AddConcavenessSeries(concavenessViewModel.ConcavenessPlot, _plan, GetStructure(ChosenStructureId));
                concavenessViewModel.ConcavenessPlot.Axes[0].Minimum = (_plan.Dose.Origin.z + strPlanes[0] * _plan.Dose.ZRes) / 10.0;
                concavenessViewModel.ConcavenessPlot.Axes[0].Maximum = (_plan.Dose.Origin.z + strPlanes[strPlanes.Count() - 1] * _plan.Dose.ZRes) / 10.0;
            }
            //concavenessViewModel2.ConcavenessPlot.Series.Add(CreateConcavenessSeries(_plan, GetStructure(ChosenStructureId)));
            //concavenessViewModel2.ConcavenessPlot.Axes[0].Minimum = (_plan.Dose.Origin.z + strPlanes[0] * _plan.Dose.ZRes) / 10.0;
            //concavenessViewModel2.ConcavenessPlot.Axes[0].Maximum = (_plan.Dose.Origin.z + strPlanes[strPlanes.Count() - 1] * _plan.Dose.ZRes) / 10.0;

            // Try to save some time by not recalculating this each time
            if (bodyDistViewModel.BodyDistPlot.Series.Count() == 0)
            {
                AddBodyDistSeries(bodyDistViewModel.BodyDistPlot, _plan, GetStructure(ChosenStructureId));
                bodyDistViewModel.BodyDistPlot.Axes[0].Minimum = (_plan.Dose.Origin.z + strPlanes[0] * _plan.Dose.ZRes) / 10.0;
                bodyDistViewModel.BodyDistPlot.Axes[0].Maximum = (_plan.Dose.Origin.z + strPlanes[strPlanes.Count() - 1] * _plan.Dose.ZRes) / 10.0;
            }

            // First plot
            firstViewModel.PlotModelOne.Title = string.Format("Plot 1: AP direction at isocenter", lineX);
            AddAnteriorBackgroundLineSeries(firstViewModel.PlotModelOne, doseInput);
            AddPosteriorBackgroundLineSeries(firstViewModel.PlotModelOne, doseInput);
            AddIsodoseDistanceAlongLineSeriesOneY(firstViewModel.PlotModelOne, _plan, GetStructure(ChosenStructureId), doseInput, "A");
            AddIsodoseDistanceAlongLineSeriesOneY(firstViewModel.PlotModelOne, _plan, GetStructure(ChosenStructureId), doseInput, "P");
            firstViewModel.setAnnotations(firstViewModel.PlotModelOne, strPlanes);

            // Second plot
            secondViewModel.PlotModelTwo.Title = string.Format("Plot 2: LR at femoral head height", lineY);
            AddLeftBackgroundLineSeries(secondViewModel.PlotModelTwo, doseInput);
            AddRightBackgroundLineSeries(secondViewModel.PlotModelTwo, doseInput);
            AddIsodoseDistanceAlongLineSeriesOneY(secondViewModel.PlotModelTwo, _plan, GetStructure(ChosenStructureId), doseInput, "L");
            AddIsodoseDistanceAlongLineSeriesOneY(secondViewModel.PlotModelTwo, _plan, GetStructure(ChosenStructureId), doseInput, "R");
            secondViewModel.setAnnotations(secondViewModel.PlotModelTwo, strPlanes);

            // Third plot
            thirdViewModel.PlotModelThree.Title = string.Format("Plot 3: LR 5cm below femoral head height", lowY);
            AddLowLeftBackgroundLineSeries(thirdViewModel.PlotModelThree, doseInput);
            AddLowRightBackgroundLineSeries(thirdViewModel.PlotModelThree, doseInput);
            AddIsodoseDistanceAlongLineSeriesOneY(thirdViewModel.PlotModelThree, _plan, GetStructure(ChosenStructureId), doseInput, "lowL");
            AddIsodoseDistanceAlongLineSeriesOneY(thirdViewModel.PlotModelThree, _plan, GetStructure(ChosenStructureId), doseInput, "lowR");
            thirdViewModel.setAnnotations(thirdViewModel.PlotModelThree, strPlanes);

            // Fourth plot
            fourthViewModel.PlotModelFour.Title = "Plot 4: Anterior of horns";
            AddHornLeftBackgroundLineSeries(fourthViewModel.PlotModelFour, doseInput);
            AddHornRightBackgroundLineSeries(fourthViewModel.PlotModelFour, doseInput);
            AddIsodoseDistanceAlongLineSeriesOneY(fourthViewModel.PlotModelFour, _plan, GetStructure(ChosenStructureId), doseInput, "hornL");
            AddIsodoseDistanceAlongLineSeriesOneY(fourthViewModel.PlotModelFour, _plan, GetStructure(ChosenStructureId), doseInput, "hornR");
            fourthViewModel.setAnnotations(fourthViewModel.PlotModelFour, strPlanes);

            
            UpdatePlots(); // this is so oxyplot updates the plotmodel
        }

        private void UpdatePlots()
        {
            firstViewModel.PlotModelOne.InvalidatePlot(true);
            secondViewModel.PlotModelTwo.InvalidatePlot(true);
            thirdViewModel.PlotModelThree.InvalidatePlot(true);
            fourthViewModel.PlotModelFour.InvalidatePlot(true);
            concavenessViewModel.ConcavenessPlot.InvalidatePlot(true);
            //concavenessViewModel2.ConcavenessPlot.InvalidatePlot(true);
            bodyDistViewModel.BodyDistPlot.InvalidatePlot(true);
        }

        // if only one of the plots is to be refreshed
        public void RefreshPlot1()
        {
            // if no dose is defined
            if (_plan.Dose == null)
            {
                System.Windows.MessageBox.Show("Plan has no dose!");
                return;
            }

            // Find structure planes to define min and max for y=0 line
            var strPlanes = FindPlanesWithStructure(_plan, GetStructure(ChosenStructureId));

            firstViewModel.PlotModelOne.Series.Clear();
            AddIsodoseDistanceAlongLineSeriesOneY(firstViewModel.PlotModelOne, _plan, GetStructure(ChosenStructureId), doseInput, "A");
            AddIsodoseDistanceAlongLineSeriesOneY(firstViewModel.PlotModelOne, _plan, GetStructure(ChosenStructureId), doseInput, "P");
            firstViewModel.setAnnotations(firstViewModel.PlotModelOne, strPlanes);
            firstViewModel.PlotModelOne.InvalidatePlot(true);
        }

        public void RefreshPlot2()
        {
            // if no dose is defined
            if (_plan.Dose == null)
            {
                System.Windows.MessageBox.Show("Plan has no dose!");
                return;
            }

            // Find structure planes to define min and max for y=0 line
            var strPlanes = FindPlanesWithStructure(_plan, GetStructure(ChosenStructureId));

            secondViewModel.PlotModelTwo.Series.Clear();
            AddIsodoseDistanceAlongLineSeriesOneY(secondViewModel.PlotModelTwo, _plan, GetStructure(ChosenStructureId), doseInput, "L");
            AddIsodoseDistanceAlongLineSeriesOneY(secondViewModel.PlotModelTwo, _plan, GetStructure(ChosenStructureId), doseInput, "R");
            secondViewModel.setAnnotations(secondViewModel.PlotModelTwo, strPlanes);
            secondViewModel.PlotModelTwo.InvalidatePlot(true);
        }

        public void RefreshPlot3()
        {
            // if no dose is defined
            if (_plan.Dose == null)
            {
                System.Windows.MessageBox.Show("Plan has no dose!");
                return;
            }

            // Find structure planes to define min and max for y=0 line
            var strPlanes = FindPlanesWithStructure(_plan, GetStructure(ChosenStructureId));

            thirdViewModel.PlotModelThree.Series.Clear();
            AddIsodoseDistanceAlongLineSeriesOneY(thirdViewModel.PlotModelThree, _plan, GetStructure(ChosenStructureId), doseInput, "lowL");
            AddIsodoseDistanceAlongLineSeriesOneY(thirdViewModel.PlotModelThree, _plan, GetStructure(ChosenStructureId), doseInput, "lowR");
            thirdViewModel.setAnnotations(thirdViewModel.PlotModelThree, strPlanes);
            thirdViewModel.PlotModelThree.InvalidatePlot(true);
        }

        public void RefreshPlot4()
        {
            // if no dose is defined
            if (_plan.Dose == null)
            {
                System.Windows.MessageBox.Show("Plan has no dose!");
                return;
            }

            // Find structure planes to define min and max for y=0 line
            var strPlanes = FindPlanesWithStructure(_plan, GetStructure(ChosenStructureId));

            fourthViewModel.PlotModelFour.Series.Clear();
            AddIsodoseDistanceAlongLineSeriesOneY(fourthViewModel.PlotModelFour, _plan, GetStructure(ChosenStructureId), doseInput, "hornL");
            AddIsodoseDistanceAlongLineSeriesOneY(fourthViewModel.PlotModelFour, _plan, GetStructure(ChosenStructureId), doseInput, "hornR");
            fourthViewModel.setAnnotations(fourthViewModel.PlotModelFour, strPlanes);
            fourthViewModel.PlotModelFour.InvalidatePlot(true);
        }
        #endregion

        #region Calculations
        /// <summary>
        /// This method creates an oxyplot areaseries. It calls another method (GetDistanceToIsodoseAlongAxis) to do the actual data calculation.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="isodoseLevel"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        private void AddIsodoseDistanceAlongLineSeriesOneY(PlotModel pm, PlanSetup plan, Structure structure, double isodoseLevel, string direction)
        {
            // get AP and LR line positions in API coordinate system
            lineX = lineXInput + _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.x;
            lineY = lineYInput + _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.y;
            lowY = lowYInput + _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.y;


            var series = new OxyPlot.Series.AreaSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id + "_" + direction;
            series.XAxisKey = "CC position [cm]";
            series.YAxisKey = "Distance to isodose [mm]";

            double centerX = lineX;
            double centerY = lineY;

            if (direction == "lowL" | direction == "lowR")
                centerY = lowY;

            for (int z = 0; z < plan.Dose.ZSize; z++)
            {
                double dist = GetDistanceToIsodoseAlongAxis(plan, structure, isodoseLevel, direction, z, centerX, centerY, spatialResInput, doseResInput);
                if (direction == "L" | direction == "A" | direction == "hornL" | direction == "lowL")
                    series.Points.Add(new DataPoint((plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0, dist));
                
                else if (direction == "R" | direction == "P" | direction == "hornR" | direction == "lowR")
                    series.Points.Add(new DataPoint((plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0, -dist));
            }

            // save anterior values to plot lines in later plots
            if (direction == "A")
            {
                Anterior = series.Points;
            }

            pm.Series.Add(series);
        }

        /// <summary>
        /// This method creates an oxyplot areaseries. It calls another method (CalculateConcaveness) to do the actual data calculation.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <returns></returns>
        private List<DataPoint> AddConcavenessSeries(PlotModel pm, PlanSetup plan, Structure structure)
        {
            // plot series to add to plot model
            var series = new OxyPlot.Series.AreaSeries();

            // list of data points to return
            List<DataPoint> cList = new List<DataPoint>();

            // series properties
            series.Tag = plan.Id;
            series.Title = plan.Id;
            series.Color = OxyColors.Gray;
            series.XAxisKey = "CC position [cm]";
            series.YAxisKey = "ConcavenessIndex";

            for (int z = 0; z < plan.Dose.ZSize; z++)
            {
                double CI = CalculateConcaveness(plan, structure, z);
                series.Points.Add(new DataPoint((plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0, CI));
                cList.Add(new DataPoint((plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0, CI));
            }
            
            pm.Series.Add(series);
            return cList;
        }

        private void AddBodyDistSeries(PlotModel model, PlanSetup plan, Structure structure)
        {
            // get AP line position in API coordinate system
            lineX = lineXInput + _plan.Beams.First(b => !b.IsSetupField).IsocenterPosition.x;

            var seriesA = new OxyPlot.Series.AreaSeries();
            seriesA.Tag = plan.Id;
            seriesA.Title = "Anterior";
            seriesA.Color = OxyColors.Gray;
            seriesA.XAxisKey = "CC position [cm]";
            seriesA.YAxisKey = "BodyDist";

            var seriesP = new OxyPlot.Series.AreaSeries();
            seriesP.Tag = plan.Id;
            seriesP.Title = "Posterior";
            seriesP.Color = OxyColors.DarkGray;
            seriesP.XAxisKey = "CC position [cm]";
            seriesP.YAxisKey = "BodyDist";

            for (int z = 0; z < plan.Dose.ZSize; z++)
            {
                double distA = GetBodyDist(plan, structure, z, lineX, "A");
                seriesA.Points.Add(new DataPoint((plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0, distA));

                double distP = GetBodyDist(plan, structure, z, lineX, "P");
                seriesP.Points.Add(new DataPoint((plan.Dose.Origin.z + z * plan.Dose.ZRes) / 10.0, distP));
            }


            model.Series.Add(seriesA);
            model.Series.Add(seriesP);
        }

        #region Background dotted lines
        private void AddAnteriorBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsBladder50(dp, isodoseLevel));
                    background75Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsBladder75(dp, isodoseLevel));
                    backgroundRP75Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsBladderRP(dp, isodoseLevel));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsBowel50(dp, isodoseLevel));
                    background75Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsBowel75(dp, isodoseLevel));
                    backgroundRP75Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsBowelRP(dp, isodoseLevel));
                }
                else
                {
                    background50Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsNone50(dp, isodoseLevel));
                    background75Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsNone75(dp, isodoseLevel));
                    backgroundRP75Series.Points.Add(LayoutFunctions.HistValsAnt.returnHistValsNoneRP(dp, isodoseLevel));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        private void AddPosteriorBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLinePost.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }


        private void AddLeftBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLR.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        private void AddRightBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLR.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        private void AddLowLeftBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineLowLR.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        private void AddLowRightBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineLowLR.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        private void AddHornLeftBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, LayoutFunctions.HistValsLineHorns.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        private void AddHornRightBackgroundLineSeries(PlotModel pm, double isodoseLevel)
        {
            // Add background dotted lines
            var background50Series = new OxyPlot.Series.LineSeries();
            background50Series.XAxisKey = "CC position [cm]";
            background50Series.YAxisKey = "Distance to isodose [mm]";
            background50Series.Tag = _plan.Id;
            background50Series.LineStyle = LineStyle.Dash;
            background50Series.Color = OxyColors.DarkSeaGreen;

            var background75Series = new OxyPlot.Series.LineSeries();
            background75Series.XAxisKey = "CC position [cm]";
            background75Series.YAxisKey = "Distance to isodose [mm]";
            background75Series.Tag = _plan.Id;
            background75Series.LineStyle = LineStyle.Dash;
            background75Series.Color = OxyColors.DarkRed;

            var backgroundRP75Series = new OxyPlot.Series.LineSeries();
            backgroundRP75Series.XAxisKey = "CC position [cm]";
            backgroundRP75Series.YAxisKey = "Distance to isodose [mm]";
            backgroundRP75Series.Tag = _plan.Id;
            backgroundRP75Series.LineStyle = LineStyle.Solid;
            backgroundRP75Series.Color = OxyColors.Blue;

            // find min and max CC values for Bladder and Bowel
            double bladderMin = (_plan.Dose.Origin.z + BladderPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bladderMax = (_plan.Dose.Origin.z + BladderPlanes.Max() * _plan.Dose.ZRes) / 10.0;
            double bowelMin = (_plan.Dose.Origin.z + BowelPlanes.Min() * _plan.Dose.ZRes) / 10.0;
            double bowelMax = (_plan.Dose.Origin.z + BowelPlanes.Max() * _plan.Dose.ZRes) / 10.0;

            foreach (var dp in Concaveness)
            {
                if (dp.X >= bladderMin & dp.X <= bladderMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsBladder50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsBladder75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsBladderRP(dp, isodoseLevel)));
                }
                else if (dp.X >= bowelMin & dp.X <= bowelMax)
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsBowel50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsBowel75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsBowelRP(dp, isodoseLevel)));
                }
                else
                {
                    background50Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsNone50(dp, isodoseLevel)));
                    background75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsNone75(dp, isodoseLevel)));
                    backgroundRP75Series.Points.Add(new DataPoint(dp.X, -LayoutFunctions.HistValsLineHorns.returnHistValsNoneRP(dp, isodoseLevel)));
                }
            }

            pm.Series.Add(background50Series);
            pm.Series.Add(background75Series);
            pm.Series.Add(backgroundRP75Series);
        }

        #endregion

        /// <summary>
        /// Method to calculate the distance from a structure's contour to a given isodose curve on a given axial plane z. Calculates the dose along a line using ESAPI's GetDoseToPoint(VVector point). The direction input must be either "A", "P", "L", "R", "lowL", "lowR", "hornL", or "hornR"
        /// </summary>
        /// <param name="plan">The current PlanSetup</param>
        /// <param name="structure">The chosen Structure</param>
        /// <param name="isodoseLevel"></param>
        /// <param name="direction">String. Must be "A", "P", "L", "R", "lowL", "lowR", "hornL", or "hornR"</param>
        /// <param name="z">Int. Image plane number</param>
        /// <param name="centerX">Double. The coordinate for the AP line along which the distance to the isodose is found.</param>
        /// <param name="centerY">Double. The coordinate for the upper LR line along which the distance to the isodose is found.</param>
        /// <param name="searchRes">Grid along which points are sampled.</param>
        /// <param name="doseRes">How close a point's dose must be to the evaluation dose to be considered on the isodose line.</param>
        /// <returns></returns>
        private static double GetDistanceToIsodoseAlongAxis(PlanSetup plan, Structure structure, double isodoseLevel, string direction, int z, double centerX, double centerY, double searchRes, double doseRes)
        {
            double dist = double.NaN;
            Dose dose = plan.Dose;
            double count = 0; // this is in order to take the average over up to five lines later

            // Get contours
            VVector[][] structureContours = structure.GetContoursOnImagePlane(z);

            // If structure has no contours on the plane return NaN
            if (structureContours.Length < 1)
            {
                return dist;
            }

            double zCoor = plan.Dose.Origin.z + z * plan.Dose.ZRes; // coordinate of the plane index

            // collect all contour points in a list. For the case that there might be several separate contours on a plane.
            var contourPoints = new List<VVector>();

            foreach (var contourArray in structureContours)
            {
                foreach (VVector point in contourArray)
                {
                    contourPoints.Add(point);
                }
            }

            // define axis along which to get the dose values

            // initiate variables
            var line = new List<VVector>();
            double minX = dose.Origin.x;
            double minY = dose.Origin.y;
            double maxX = dose.Origin.x + (dose.XSize * dose.XRes);
            double maxY = dose.Origin.y + (dose.YSize * dose.YRes);
            VVector pointOnLine = new VVector();

            // find min and max x and y values for segment definition
            double maxYLoc = contourPoints.First().y;
            double minYLoc = contourPoints.First().y;
            double maxXLoc = contourPoints.First().x;
            double minXLoc = contourPoints.First().x;

            foreach (VVector point in contourPoints) // for each point in the current contour segment
            {
                if (point.y < minYLoc)
                {
                    minYLoc = point.y;
                }
                else if (point.y > maxYLoc)
                {
                    maxYLoc = point.y;
                }

                if (point.x < minXLoc)
                {
                    minXLoc = point.x;
                }
                else if (point.x > maxXLoc)
                {
                    maxXLoc = point.x;
                }
            }


            // Anterior
            if (direction.ToUpper() == "A")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // contour points on the line

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(centerX + k, profileStart, zCoor), new VVector(centerX + k, profileEnd, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found. Saves a lot of time.
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double y = pointOnLine.y; y > minY; y -= searchRes)
                    {
                        VVector point = new VVector(pointOnLine.x, y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance2D(point.x, pointOnLine.x, point.y, pointOnLine.y);
                            count += 1;
                            break;
                        }
                    }
                }
            }
            // Posterior
            else if (direction.ToUpper() == "P")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(centerX + k, profileStart, zCoor), new VVector(centerX + k, profileEnd, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = segment.Count() - 1; i > 0; i--)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break;
                        }
                    }

                    // if the structure does not lie on the search line
                    if (!found)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double y = pointOnLine.y; y < maxY; y += searchRes)
                    {
                        VVector point = new VVector(pointOnLine.x, y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance2D(point.x, pointOnLine.x, point.y, pointOnLine.y);
                            count += 1;
                            break;
                        }
                    }
                }
            }
            // Left. Upper and lower.
            else if (direction.ToUpper() == "L" | direction == "lowL")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxXLoc) + 10);
                    int profileStart = (int)(Math.Floor(minXLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(profileStart, centerY + k, zCoor), new VVector(profileEnd, centerY + k, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = segment.Count() - 1; i > 0; i--)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break;
                        }
                    }
                    // if the structure does not lie on the search line
                    if (!found | pointOnLine.x < centerX)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double x = pointOnLine.x; x < maxX; x += searchRes)
                    {
                        VVector point = new VVector(x, pointOnLine.y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance2D(point.x, pointOnLine.x, point.y, pointOnLine.y);
                            count += 1;
                            break;
                        }
                    }
                }
            }
            // right. Upper and lower.
            else if (direction.ToUpper() == "R" | direction == "lowR")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxXLoc) + 10);
                    int profileStart = (int)(Math.Floor(minXLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(profileStart, centerY + k, zCoor), new VVector(profileEnd, centerY + k, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break;
                        }
                    }

                    // if structure does not cross line return zero
                    if (!found | pointOnLine.x > centerX)
                    {
                        dist += 0;
                        continue;
                    }

                    // search points and stop when desired dose is reached. 
                    for (double x = pointOnLine.x; x > minX; x -= searchRes)
                    {
                        VVector point = new VVector(x, pointOnLine.y, pointOnLine.z);
                        double pointDose = dose.GetDoseToPoint(point).Dose;
                        if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                        {
                            dist += GetDistance2D(point.x, pointOnLine.x, point.y, pointOnLine.y);
                            count += 1;
                            break;
                        }
                    }
                }
            }
            // PTV "horn" left
            else if (direction == "hornL")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 1)
                {
                    // contour points on the line

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm

                    // find x values of the "horns"

                    var leftPoints = contourPoints.Where(po => po.x > centerX);
                    if (leftPoints.Count() > 0)
                    {
                        var maxYPoint = leftPoints.MinBy(po => po.y).First();

                        // get segment profile
                        var segment = structure.GetSegmentProfile(new VVector(maxYPoint.x + k, profileStart, zCoor), new VVector(maxYPoint.x + k, profileEnd, zCoor), segmentBuffer);
                        bool found = false;

                        for (int i = 0; i < segment.Count(); i++)
                        {
                            if (segment[i].Value)
                            {
                                pointOnLine = segment[i].Position;
                                found = true;
                                break;
                            }
                        }
                        // if the structure does not lie on the search line
                        if (!found)
                        {
                            dist += 0;
                            continue;
                        }

                        // search points and stop when desired dose is reached. 
                        for (double y = pointOnLine.y; y > minY; y -= searchRes)
                        {
                            VVector point = new VVector(pointOnLine.x, y, pointOnLine.z);
                            double pointDose = dose.GetDoseToPoint(point).Dose;
                            if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                            {
                                dist += GetDistance2D(point.x, pointOnLine.x, point.y, pointOnLine.y);
                                count += 1;
                                break;
                            }
                        }
                    }
                }
            }
            // PTV "horn" right
            else if (direction == "hornR")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 1)
                {
                    // contour points on the line

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm

                    // find x values of the "horns"

                    var rightPoints = contourPoints.Where(po => po.x < centerX);

                    if (rightPoints.Count() > 0)
                    {
                        var maxYPoint = rightPoints.MinBy(po => po.y).First();

                        // get segment profile
                        var segment = structure.GetSegmentProfile(new VVector(maxYPoint.x + k, profileStart, zCoor), new VVector(maxYPoint.x + k, profileEnd, zCoor), segmentBuffer);
                        bool found = false;

                        for (int i = 0; i < segment.Count(); i++)
                        {
                            if (segment[i].Value)
                            {
                                pointOnLine = segment[i].Position;
                                found = true;
                                break;
                            }
                        }
                        // if the structure does not lie on the search line
                        if (!found)
                        {
                            dist += 0;
                            continue;
                        }

                        // search points and stop when desired dose is reached. 
                        for (double y = pointOnLine.y; y > minY; y -= searchRes)
                        {
                            VVector point = new VVector(pointOnLine.x, y, pointOnLine.z);
                            double pointDose = dose.GetDoseToPoint(point).Dose;
                            if (Math.Abs(pointDose - isodoseLevel) < doseRes)
                            {
                                dist += GetDistance2D(point.x, pointOnLine.x, point.y, pointOnLine.y);
                                count += 1;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Direction must be either A, P, L, R, lowL, lowR, hornL, or hornR!"); // I guess this is superfluous since I set this in the code. Seemed like a good idea at the time. Keeping it just in case.
                return dist;
            }
            if (count > 0) // to avoid dividing by zero and returning NaN if PTV is not on the line
            {
                return dist / count;
            }
            else
            {
                return dist;
            }
        }

        /// <summary>
        /// This is to give the planner an idea on the PTV's shape in the plot. Concaveness is here defined as the width between the two "horn" top points (taken as the max left and max right point on each single slice) divided by the height of the concavity (defined as the y-difference between the anterior centerX point and the anterior-most contour point.
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="z">Int Image plane number</param>
        /// <returns></returns>
        private double CalculateConcaveness(PlanSetup plan, Structure structure, int z)
        {
            double dist = double.NaN;
            var dose = plan.Dose;

            // Get contours
            VVector[][] structureContours = structure.GetContoursOnImagePlane(z);

            // If structure has no contours on the plane return
            if (structureContours.Length < 1)
            {
                return dist;
            }

            double zCoor = plan.Dose.Origin.z + z * plan.Dose.ZRes;

            // collect all contour points in a list. For the case that there might be several separate contours on a plane.
            var contourPoints = new List<VVector>();

            foreach (var contourArray in structureContours)
            {
                foreach (VVector point in contourArray)
                {
                    contourPoints.Add(point);
                }
            }

            // initiate variables
            var line = new List<VVector>();
            double minX = dose.Origin.x;
            double minY = dose.Origin.y;
            double maxX = dose.Origin.x + (dose.XSize * dose.XRes);
            double maxY = dose.Origin.y + (dose.YSize * dose.YRes);
            VVector pointOnLine = new VVector();

            // find min and max x and y values for segment definition
            double maxYLoc = contourPoints.First().y;
            double minYLoc = contourPoints.First().y;
            double maxXLoc = contourPoints.First().x;
            double minXLoc = contourPoints.First().x;

            foreach (VVector point in contourPoints) // for each point in the current contour segment
            {
                if (point.y < minYLoc)
                {
                    minYLoc = point.y;
                }
                else if (point.y > maxYLoc)
                {
                    maxYLoc = point.y;
                }

                if (point.x < minXLoc)
                {
                    minXLoc = point.x;
                }
                else if (point.x > maxXLoc)
                {
                    maxXLoc = point.x;
                }
            }

            double centerX = minXLoc + (Math.Abs(maxXLoc - minXLoc) / 2.0);

            // contour points on the x=0 line

            // initiate buffer for segment profile
            int profileEnd = (int)(Math.Ceiling(maxYLoc) + 5);
            int profileStart = (int)(Math.Floor(minYLoc) - 5);
            System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


            // get segment profile
            var segment = structure.GetSegmentProfile(new VVector(centerX, profileStart, zCoor), new VVector(centerX, profileEnd, zCoor), segmentBuffer);
            bool found = false;

            for (int i = 0; i < segment.Count(); i++)
            {
                if (segment[i].Value)
                {
                    pointOnLine = segment[i].Position;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                return 0;
            }

            //// y distance between x=0 point and maxY point

            // height should be defined as AP distance from center of PTV to the "lowest" of the two horns
            // find y values of the "horns"

            var leftPoints = contourPoints.Where(po => po.x > centerX);
            var rightPoints = contourPoints.Where(po => po.x < centerX);

            // if the structure does not cross the centerX line in a given slice
            if (leftPoints.Count() < 1 | rightPoints.Count() < 1)
            {
                return 0;
            }

            var maxYPointLeft = leftPoints.MinBy(po => po.y).First();
            var maxYPointRight = rightPoints.MinBy(po => po.y).First();

            double height = 0;
            if (maxYPointLeft.y < maxYPointRight.y) // target's most anterior position is on patient's left side
            {
                height = Math.Abs(pointOnLine.y - maxYPointRight.y);
            }
            else
            {
                height = Math.Abs(pointOnLine.y - maxYPointLeft.y);
            }

            // if height is less than 1mm, target is defined as round
            if (height < 1)
            {
                return 0;
            }

            // define width at half-maximum height
            // find left point at this height
            System.Collections.BitArray leftBuffer = new System.Collections.BitArray((int)Math.Floor(Math.Abs((maxX - centerX))) * 10);
            var segmentLeft = structure.GetSegmentProfile(new VVector(centerX, pointOnLine.y - (height / 2.0), zCoor), new VVector(maxX, pointOnLine.y + (height / 2.0), zCoor), leftBuffer);
            VVector leftPoint = new VVector();

            found = false;
            foreach (var segPoint in segmentLeft)
            {
                if(segPoint.Value) // first structure point the segment hits
                {
                    leftPoint = segPoint.Position;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                return 0;
            }

            // find right point at this height
            System.Collections.BitArray rightBuffer = new System.Collections.BitArray((int)Math.Floor(Math.Abs((minX - centerX))) * 10);
            var segmentRight = structure.GetSegmentProfile(new VVector(centerX, pointOnLine.y - (height / 2.0), zCoor), new VVector(minX, pointOnLine.y + (height / 2.0), zCoor), leftBuffer);
            VVector rightPoint = new VVector();

            found = false;
            foreach (var segPoint in segmentRight)
            {
                if (segPoint.Value) // first structure point the segment hits
                {
                    rightPoint = segPoint.Position;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                return 0;
            }
            double width = Math.Abs(leftPoint.x - rightPoint.x);

            return height / width; // the higher and narrower the concavity is, the harder it is to press dose down in it. Higher index: harder to optimize.
        }

        private double GetBodyDist(PlanSetup plan, Structure structure, int z, double centerX, string dir)
        {
            double dist = double.NaN;
            Dose dose = plan.Dose;
            double count = 0; // this is in order to take the average over up to five lines later

            // Get PTV contours
            VVector[][] structureContours = structure.GetContoursOnImagePlane(z);

            // Get Body contours
            Structure body = plan.StructureSet.Structures.First(s => s.Id.ToUpper() == "BODY");
            VVector[][] bodyContours = body.GetContoursOnImagePlane(z);

            // If structure has no contours on the plane return NaN
            if (structureContours.Length < 1)
            {
                return dist;
            }

            double zCoor = plan.Dose.Origin.z + z * plan.Dose.ZRes; // coordinate of the plane index

            // collect all contour points in a list. For the case that there might be several separate contours on a plane.
            var contourPoints = new List<VVector>();

            foreach (var contourArray in structureContours)
            {
                foreach (VVector point in contourArray)
                {
                    contourPoints.Add(point);
                }
            }

            var bodyPoints = new List<VVector>();

            foreach (var bodyArray in bodyContours)
            {
                foreach (VVector point in bodyArray)
                {
                    bodyPoints.Add(point);
                }
            }

            // define axis along which to get the dose values

            // initiate variables
            var line = new List<VVector>();
            var bodyLine = new List<VVector>();
            double minX = dose.Origin.x;
            double minY = dose.Origin.y;
            double maxX = dose.Origin.x + (dose.XSize * dose.XRes);
            double maxY = dose.Origin.y + (dose.YSize * dose.YRes);
            VVector pointOnLine = new VVector();
            VVector bodyPointOnLine = new VVector();

            // find min and max x and y values for segment definition
            double maxYLoc = contourPoints.First().y;
            double minYLoc = contourPoints.First().y;


            foreach (VVector point in contourPoints) // for each point in the current contour segment
            {
                if (point.y < minYLoc)
                {
                    minYLoc = point.y;
                }
                else if (point.y > maxYLoc)
                {
                    maxYLoc = point.y;
                }

            }

            // find min and max x and y values for segment definition
            double bodyMaxYLoc = bodyPoints.First().y;
            double bodyMinYLoc = bodyPoints.First().y;


            foreach (VVector point in bodyPoints) // for each point in the current contour segment
            {
                if (point.y < bodyMinYLoc)
                {
                    bodyMinYLoc = point.y;
                }
                else if (point.y > bodyMaxYLoc)
                {
                    bodyMaxYLoc = point.y;
                }

            }

            // Anterior
            if (dir.ToUpper() == "A")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // contour points on the line

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(centerX + k, profileStart, zCoor), new VVector(centerX + k, profileEnd, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = 0; i < segment.Count(); i++)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found. Saves a lot of time.
                        }
                    }

                    // body points on the line

                    // initiate buffer for segment profile
                    int bodyProfileEnd = (int)(Math.Floor(bodyMaxYLoc) + 10);
                    int bodyProfileStart = (int)(Math.Floor(bodyMinYLoc) - 10);
                    System.Collections.BitArray bodySegmentBuffer = new System.Collections.BitArray((bodyProfileEnd - bodyProfileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var bodySegment = body.GetSegmentProfile(new VVector(centerX + k, bodyProfileStart, zCoor), new VVector(centerX + k, bodyProfileEnd, zCoor), segmentBuffer);
                    bool bodyFound = false;

                    for (int i = 0; i < bodySegment.Count(); i++)
                    {
                        if (bodySegment[i].Value)
                        {
                            bodyPointOnLine = segment[i].Position;
                            bodyFound = true;
                            break; // stop search when a point is found. Saves a lot of time.
                        }
                    }

                    if (found & bodyFound)
                    {
                        //dist += GetDistance2D(pointOnLine.x, bodyPointOnLine.x, pointOnLine.y, bodyPointOnLine.y);
                        double distance = GetDistance2D(pointOnLine.x, bodyPointOnLine.x, pointOnLine.y, bodyPointOnLine.y);

                        VVector start = pointOnLine;
                        VVector stop = bodyPointOnLine;

                        double[] buffer = new double[(int)Math.Ceiling(distance)]; // get resolution as close to 1mm as possible
                        var HUprofile = image.GetImageProfile(start, stop, buffer);

                        foreach (var point in HUprofile)
                        {
                            dist += (point.Value / 1000 + 1) * distance / Math.Ceiling(distance); // approximation of water equivalent path length. distance / ceil(distance) is the length of each profile voxel.
                        }
                        count += 1;
                    }
                }
                return dist / count;
            }

            // Posterior
            if (dir.ToUpper() == "P")
            {
                dist = 0;

                // take average over five adjacent lines to increase robustness
                for (double k = -1; k <= 1; k += 0.5)
                {
                    // contour points on the line

                    // initiate buffer for segment profile
                    int profileEnd = (int)(Math.Ceiling(maxYLoc) + 10);
                    int profileStart = (int)(Math.Floor(minYLoc) - 10);
                    System.Collections.BitArray segmentBuffer = new System.Collections.BitArray((profileEnd - profileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var segment = structure.GetSegmentProfile(new VVector(centerX + k, profileStart, zCoor), new VVector(centerX + k, profileEnd, zCoor), segmentBuffer);
                    bool found = false;

                    for (int i = segment.Count() - 1; i > 0; i--)
                    {
                        if (segment[i].Value)
                        {
                            pointOnLine = segment[i].Position;
                            found = true;
                            break; // stop search when a point is found. Saves a lot of time.
                        }
                    }

                    

                    // body points on the line

                    // initiate buffer for segment profile
                    int bodyProfileEnd = (int)(Math.Floor(bodyMaxYLoc) + 10);
                    int bodyProfileStart = (int)(Math.Floor(bodyMinYLoc) - 10);
                    System.Collections.BitArray bodySegmentBuffer = new System.Collections.BitArray((bodyProfileEnd - bodyProfileStart) * 10); // resolution : 0.1mm


                    // get segment profile
                    var bodySegment = body.GetSegmentProfile(new VVector(centerX + k, bodyProfileStart, zCoor), new VVector(centerX + k, bodyProfileEnd, zCoor), segmentBuffer);
                    bool bodyFound = false;

                    for (int i = bodySegment.Count() - 1; i > 0; i--)
                    {
                        if (bodySegment[i].Value)
                        {
                            bodyPointOnLine = segment[i].Position;
                            bodyFound = true;
                            break; // stop search when a point is found. Saves a lot of time.
                        }
                    }

                    if (found & bodyFound)
                    {
                        //dist += GetDistance2D(pointOnLine.x, bodyPointOnLine.x, pointOnLine.y, bodyPointOnLine.y);
                        double distance = GetDistance2D(pointOnLine.x, bodyPointOnLine.x, pointOnLine.y, bodyPointOnLine.y);

                        VVector start = pointOnLine;
                        VVector stop = bodyPointOnLine;

                        double[] buffer = new double[(int)Math.Ceiling(distance)]; // get resolution as close to 1mm as possible
                        var HUprofile = image.GetImageProfile(start, stop, buffer);

                        foreach (var point in HUprofile)
                        {
                            dist += (point.Value / 1000 + 1) * distance / Math.Ceiling(distance); // approximation of water equivalent path length. distance / ceil(distance) is the length of each profile voxel.
                        }

                        count += 1;
                    }
                }
                return -dist / count;
            }
            else
            {
                return dist;
            }
        }

        #endregion
        #region Helper functions
        // find image planes containing the given structure
        private List<double> FindPlanesWithStructure(PlanSetup plan, Structure structure)
        {
            List<double> planes = new List<double>();

            if (plan.StructureSet.Structures.Any(s => (s.Id.ToUpper() == structure.Id.ToUpper() & !s.IsEmpty)))
            {
                for (int z = 0; z < plan.Dose.ZSize; z++)
                {
                    if (structure.GetContoursOnImagePlane(z).Length > 0)
                    {
                        planes.Add(z);
                    }
                }
            }
            return planes;
        }
        // overload for string ID input
        private List<double> FindPlanesWithStructure(PlanSetup plan, string structureID)
        {
            List<double> planes = new List<double>();

            if (plan.StructureSet.Structures.Any(s => s.Id.ToUpper() == structureID.ToUpper()))
            {
                Structure structure = plan.StructureSet.Structures.First(s => (s.Id.ToUpper() == structureID.ToUpper() & !s.IsEmpty));
                for (int z = 0; z < plan.Dose.ZSize; z++)
                {
                    if (structure.GetContoursOnImagePlane(z).Length > 0)
                    {
                        planes.Add(z);
                    }
                }
            }
            return planes;
        }

        // find midpoint of femoral heads to set default LR y-line value
        private double FindFemoralHeadMidY(PlanSetup plan)
        {
            // If femoral heads are not defined or are incorrectly named
            if (!plan.StructureSet.Structures.Any(s => s.Id == "Femoral H L") | !plan.StructureSet.Structures.Any(s => s.Id == "Femoral H R"))
            {
                System.Windows.MessageBox.Show("Could not find at least one femoral head structure. Returning default value of y = 45mm.");
                return 45;
            }

            Structure Fem_L = plan.StructureSet.Structures.First(s => s.Id == "Femoral H L");
            Structure Fem_R = plan.StructureSet.Structures.First(s => s.Id == "Femoral H R");
            VVector center_L = Fem_L.CenterPoint;
            VVector center_R = Fem_R.CenterPoint;

            return (center_L.y + center_R.y) / 2.0;
        }

        // calculate euclidian distance between two points on a plane
        private static double GetDistance2D(double x1, double x2, double y1, double y2)
        {
            double dist = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
            return dist;
        }

        // this is to make life easier for myself in the project starting period. Might be a nice feature for later use as well
        public void ExportDVHMetrics(string path)
        {
            // lists of possible IDs for OARs. in case of misspellings/non-conformity with nomenclature standards
            List<string> bowelIDs = new List<string> { "bowel_cavity", "bowel cavity", "Bowel_cavity" };
            List<string> bladderIDs = new List<string> { "Bladder", "bladder" };
            List<string> fhlIDs = new List<string> { "Femoral H L"  };
            List<string> fhrIDs = new List<string> { "Femoral H R" };
            List<string> sacrumIDs = new List<string> { "Os sacrum", "os sacrum", "Os Sacrum" };
            List<string> penisIDs = new List<string> { "bulbus penis", "Bulbus penis", "Bulbus Penis" };
            List<string> labiaIDs = new List<string> { "labia", "Labia" };


            // write to file
            using (var sw = new StreamWriter(path))
            {
                _plan.DoseValuePresentation = DoseValuePresentation.Absolute;
                
                sw.WriteLine("Metric;Value;Unit;;Objective");
                sw.WriteLine();

                // clinical OAR objectives taken from medfys wiki
                if (_plan.StructureSet.Structures.Any(s => bowelIDs.Contains(s.Id))) 
                {
                    Structure bowel = _plan.StructureSet.Structures.FirstOrDefault(s => bowelIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bowel V_30Gy", _plan.GetVolumeAtDose(bowel, new DoseValue(30, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc","<600cc"));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bowel V_45Gy", _plan.GetVolumeAtDose(bowel, new DoseValue(45, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc", "<300cc"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bowel V_30Gy", double.NaN, "cc", "<600cc"));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bowel V_45Gy", double.NaN, "cc", "<300cc"));
                }

                if (_plan.StructureSet.Structures.Any(s => bladderIDs.Contains(s.Id))) 
                {
                    Structure bladder = _plan.StructureSet.Structures.FirstOrDefault(s => bladderIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bladder V_50Gy", _plan.GetVolumeAtDose(bladder, new DoseValue(50, DoseValue.DoseUnit.Gy), VolumePresentation.Relative), "%", "<20%"));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bladder V_35Gy", _plan.GetVolumeAtDose(bladder, new DoseValue(35, DoseValue.DoseUnit.Gy), VolumePresentation.Relative), "%", "<75%"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bladder V_50Gy", double.NaN, "%", "<20%"));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bladder V_35Gy", double.NaN, "%", "<75%"));
                }

                if (_plan.StructureSet.Structures.Any(s => fhlIDs.Contains(s.Id)))
                {
                    Structure FHl = _plan.StructureSet.Structures.First(s => fhlIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "FH L V_50Gy", _plan.GetVolumeAtDose(FHl, new DoseValue(50, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc", "<1cc"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "FH L V_50Gy", double.NaN, "cc", "<1cc"));
                }

                if (_plan.StructureSet.Structures.Any(s => fhrIDs.Contains(s.Id)))
                {
                    Structure FHr = _plan.StructureSet.Structures.First(s => fhrIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "FH R V_50Gy", _plan.GetVolumeAtDose(FHr, new DoseValue(50, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc", "<1cc"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "FH R V_50Gy", double.NaN, "cc", "<1cc"));
                }

                if (_plan.StructureSet.Structures.Any(s => sacrumIDs.Contains(s.Id)))
                {
                    Structure sacrum = _plan.StructureSet.Structures.First(s => sacrumIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Os sacrum V_50Gy", _plan.GetVolumeAtDose(sacrum, new DoseValue(50, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc", "mest muligt"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Os sacrum V_50Gy", double.NaN, "cc", "mest muligt"));
                }

                if (_plan.StructureSet.Structures.Any(s => penisIDs.Contains(s.Id)))
                {
                    Structure penis = _plan.StructureSet.Structures.First(s => penisIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bulbus penis V_50Gy", _plan.GetVolumeAtDose(penis, new DoseValue(50, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc", "mest muligt"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Bulbus penis V_50Gy", double.NaN, "cc", "mest muligt"));
                }

                if (_plan.StructureSet.Structures.Any(s => labiaIDs.Contains(s.Id)))
                {
                    Structure labia = _plan.StructureSet.Structures.First(s => labiaIDs.Contains(s.Id));
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Labia V_50Gy", _plan.GetVolumeAtDose(labia, new DoseValue(50, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3), "cc", "mest muligt"));
                }
                else
                {
                    sw.WriteLine(string.Format("{0};{1};{2};;{3}", "Labia V_50Gy", double.NaN, "cc", "mest muligt"));
                }
            }
        }
        #endregion
    }
}
