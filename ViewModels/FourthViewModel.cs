﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace RectumDirectionalGradientManyViews.ViewModels
{
    public class FourthViewModel
    {
        // public fields
        public PlotModel PlotModelFour { get; private set; }
        private PlanSetup _plan;

        //Constructor
        public FourthViewModel(ScriptContext context)
        {
            PlotModelFour = new PlotModel();
            _plan = context.PlanSetup;

            setAxes(PlotModelFour);
            setLegend(PlotModelFour);
        }

        #region Axes etc
        private void setAxes(PlotModel pm)
        {
            // hidden category y axis
            pm.Axes.Add(new OxyPlot.Axes.CategoryAxis
            {
                IsAxisVisible = false,
                Minimum = 0,
                Maximum = 30,
                Key = "Hidden YAxis",
                Position = AxisPosition.Left
            });

            // x-axis
            var xaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "CC position [cm]",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 10,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                Key = "CC position [cm]"
            };

            // y-axis
            var yaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "Distance to isodose [mm]",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 10,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                Key = "Distance to isodose [mm]"
            };

            pm.Axes.Add(xaxis);
            pm.Axes.Add(yaxis);
        }

        private void setLegend(PlotModel pm)
        {
            pm.LegendPlacement = LegendPlacement.Outside;
            pm.LegendBorder = OxyColors.Black;
            pm.LegendBackground = OxyColor.FromAColor(32, OxyColors.Black);
            pm.LegendPosition = LegendPosition.BottomCenter;
            pm.LegendOrientation = LegendOrientation.Horizontal;
            pm.IsLegendVisible = true;
        }
        #endregion
        #region set annotations etc
        public void setAnnotations(PlotModel pm, List<double> strPlanes)
        {
            CommonMethods.AddStructureBarAnnotation(PlotModelFour, _plan, new string[] { "Bladder", "o PTV concav", "Bowel_cavity" }, new OxyColor[] { OxyColors.Yellow, OxyColors.LightGreen, OxyColors.DarkGreen });
            pm.Series.Add(CommonMethods.CreateHorizontalLine(_plan, 0, "Distance to isodose [mm]", "CC position [cm]", (_plan.Dose.Origin.z + strPlanes[0] * _plan.Dose.ZRes) / 10.0, (_plan.Dose.Origin.z + strPlanes[strPlanes.Count() - 1] * _plan.Dose.ZRes) / 10.0));
            pm.Axes[2].MinimumPadding = 0.1;
            pm.Axes[1].Minimum = (_plan.Dose.Origin.z + strPlanes[0] * _plan.Dose.ZRes) / 10.0;
            pm.Axes[1].Maximum = (_plan.Dose.Origin.z + strPlanes[strPlanes.Count() - 1] * _plan.Dose.ZRes) / 10.0;
        }
        #endregion
        #region export logic
        public void ExportPlotAsPdf(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                OxyPlot.Pdf.PdfExporter.Export(PlotModelFour, stream, 600, 400);
            }
        }



        // Export plotted data as txt file
        public void ExportDataAsTxt(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in PlotModelFour.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.AreaSeries().GetType()) )
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        sw.WriteLine(string.Format("#{0}\t{1}", PlotModelFour.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Bottom).Title, PlotModelFour.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Left).Title));
                        var dataSer = (OxyPlot.Series.DataPointSeries)ser;
                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X.ToString(), point.Y.ToString()));
                        }
                        sw.WriteLine(string.Format("\n\n"));
                    }
                    else if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.IntervalBarSeries().GetType())) // include OAR information
                    {
                        IntervalBarSeries intBarSer = (IntervalBarSeries)ser;
                        foreach (var item in intBarSer.Items)
                        {
                            if (item.Color == OxyColors.Yellow)
                            {
                                sw.WriteLine(string.Format("#Bladder start\tBladder end [CC position in mm]"));
                                sw.WriteLine(string.Format("{0}\t{1}\n\n", item.Start, item.End));
                            }
                            else if (item.Color == OxyColors.DarkGreen)
                            {
                                sw.WriteLine(string.Format("#Bowel start\tBowel end [CC position in mm]"));
                                sw.WriteLine(string.Format("{0}\t{1}\n\n", item.Start, item.End));
                            }
                            else if (item.Color == OxyColors.LightGreen)
                            {
                                sw.WriteLine(string.Format("#oPTVconcav start\toPTVconcav end [CC position in mm]"));
                                sw.WriteLine(string.Format("{0}\t{1}\n\n", item.Start, item.End));
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
