﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace RectumDirectionalGradientManyViews
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        // Fields
        private readonly ViewModels.MainViewModel _vm;
        //private readonly string currentPath;

        public MainView(ViewModels.MainViewModel viewModel)
        {
            _vm = viewModel;

            InitializeComponent();
            DataContext = _vm;

            // Load variables from previous session
            LoadSavedVariables();

            PlotOne.DataContext = _vm.firstViewModel;
            PlotTwo.DataContext = _vm.secondViewModel;
            PlotThree.DataContext = _vm.thirdViewModel;
            PlotFour.DataContext = _vm.fourthViewModel;
            ConcavenessPlot.DataContext = _vm.concavenessViewModel;
            //ConcavenessPlot2.DataContext = _vm.concavenessViewModel2;
            ConcavenessPlot2.DataContext = _vm.bodyDistViewModel;
        }

        private void LoadSavedVariables()
        {
            // Get default start variable values from user settings
            _vm.doseInput = Properties.Settings.Default.chosenDoseSetting;
            _vm.doseResInput = Properties.Settings.Default.chosenDoseResSetting;
            _vm.spatialResInput = Properties.Settings.Default.chosenSpatialResSetting;
            _vm.ChosenStructureId = Properties.Settings.Default.chosenStructureSetting;

            EnterDoseBox.Text = Properties.Settings.Default.chosenDoseSetting.ToString();
            DoseResBox.Text = Properties.Settings.Default.chosenDoseResSetting.ToString();
            SpatialResBox.Text = Properties.Settings.Default.chosenSpatialResSetting.ToString();
            Box1.Text = Convert.ToString(Math.Round(_vm.lineXInput, 2));
            Box2.Text = Convert.ToString(Math.Round(_vm.lineYInput, 2));
            Box3.Text = Convert.ToString(Math.Round(_vm.lowYInput, 2));
        }


        #region Textbox and button logic
        private void ClickButton(object sender, RoutedEventArgs e)
        {

            // Read input from textboxes
            var doseEntry = EnterDoseBox.Text;
            var spatialResEntry = SpatialResBox.Text;
            var doseResEntry = DoseResBox.Text;
            var xEntry = Box1.Text;
            var y1Entry = Box2.Text;
            var y2Entry = Box3.Text;

            // If searchres is defined too small the code might take too long to run
            if (IsDecimal(spatialResEntry))
            {
                double val = Convert.ToDouble(spatialResEntry);
                if (val < 0.001)
                {
                    MessageBox.Show("Spatial search grid resolution is set too low. Please input a value of at least 0.001.");
                    return;
                }
            }


            // clear previous plots
            _vm.ClearGradients();

            // Check if entries are numerical. If they are show plots.
            if (IsDecimal(doseEntry) & IsDecimal(spatialResEntry) & IsDecimal(doseResEntry) & IsDecimal(xEntry) & IsDecimal(y1Entry) & IsDecimal(y2Entry))
            {
                _vm.doseInput = Convert.ToDouble(doseEntry);
                _vm.spatialResInput = Convert.ToDouble(spatialResEntry);
                _vm.doseResInput = Convert.ToDouble(doseResEntry);
                _vm.lineXInput = Math.Round(Convert.ToDouble(xEntry), 2);
                Box1.Text = _vm.lineXInput.ToString();
                _vm.lineYInput = Math.Round(Convert.ToDouble(y1Entry), 2);
                Box2.Text = _vm.lineYInput.ToString();
                _vm.lowYInput = Math.Round(Convert.ToDouble(y2Entry), 2);
                Box3.Text = _vm.lowYInput.ToString();
                _vm.ShowPlots();

                
                // save values to user settings
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(spatialResEntry);
                Properties.Settings.Default.chosenDoseResSetting = Convert.ToDouble(doseResEntry);
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(doseEntry);
            }
            else
                MessageBox.Show("Inputs must all be numerical values!");

            // Export data automatically
            // Comment this beofre submitting!
            // AutoExportAll();
        }

        private void ClickPlot1(object sender, RoutedEventArgs e)
        {
            // Read input from textboxes
            var doseEntry = EnterDoseBox.Text;
            var spatialResEntry = SpatialResBox.Text;
            var doseResEntry = DoseResBox.Text;
            var xEntry = Box1.Text;
            var y1Entry = Box2.Text;
            var y2Entry = Box3.Text;

            // If searchres is defined too small the code might take too long to run
            if (IsDecimal(spatialResEntry))
            {
                double val = Convert.ToDouble(spatialResEntry);
                if (val < 0.001)
                {
                    MessageBox.Show("Spatial search grid resolution is set too low. Please input a value of at least 0.001.");
                    return;
                }
            }

            // Check if entries are numerical. If they are show plots.
            if (IsDecimal(doseEntry) & IsDecimal(spatialResEntry) & IsDecimal(doseResEntry) & IsDecimal(xEntry) & IsDecimal(y1Entry) & IsDecimal(y2Entry))
            {
                _vm.doseInput = Convert.ToDouble(doseEntry);
                _vm.spatialResInput = Convert.ToDouble(spatialResEntry);
                _vm.doseResInput = Convert.ToDouble(doseResEntry);
                _vm.lineXInput = Math.Round(Convert.ToDouble(xEntry), 2);
                Box1.Text = _vm.lineXInput.ToString();
                _vm.lineYInput = Math.Round(Convert.ToDouble(y1Entry), 2);
                Box2.Text = _vm.lineYInput.ToString();
                _vm.lowYInput = Math.Round(Convert.ToDouble(y2Entry), 2);
                Box3.Text = _vm.lowYInput.ToString();
                _vm.RefreshPlot1();

                // save values to user settings
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(spatialResEntry);
                Properties.Settings.Default.chosenDoseResSetting = Convert.ToDouble(doseResEntry);
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(doseEntry);

            }
            else
                MessageBox.Show("Inputs must all be numerical values!");
        }

        private void ClickPlot2(object sender, RoutedEventArgs e)
        {
            // Read input from textboxes
            var doseEntry = EnterDoseBox.Text;
            var spatialResEntry = SpatialResBox.Text;
            var doseResEntry = DoseResBox.Text;
            var xEntry = Box1.Text;
            var y1Entry = Box2.Text;
            var y2Entry = Box3.Text;

            // If searchres is defined too small the code might take too long to run
            if (IsDecimal(spatialResEntry))
            {
                double val = Convert.ToDouble(spatialResEntry);
                if (val < 0.001)
                {
                    MessageBox.Show("Spatial search grid resolution is set too low. Please input a value of at least 0.001.");
                    return;
                }
            }

            // Check if entries are numerical. If they are show plots.
            if (IsDecimal(doseEntry) & IsDecimal(spatialResEntry) & IsDecimal(doseResEntry) & IsDecimal(xEntry) & IsDecimal(y1Entry) & IsDecimal(y2Entry))
            {
                _vm.doseInput = Convert.ToDouble(doseEntry);
                _vm.spatialResInput = Convert.ToDouble(spatialResEntry);
                _vm.doseResInput = Convert.ToDouble(doseResEntry);
                _vm.lineXInput = Math.Round(Convert.ToDouble(xEntry), 2);
                Box1.Text = _vm.lineXInput.ToString();
                _vm.lineYInput = Math.Round(Convert.ToDouble(y1Entry), 2);
                Box2.Text = _vm.lineYInput.ToString();
                _vm.lowYInput = Math.Round(Convert.ToDouble(y2Entry), 2);
                Box3.Text = _vm.lowYInput.ToString();
                _vm.RefreshPlot2();

                // save values to user settings
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(spatialResEntry);
                Properties.Settings.Default.chosenDoseResSetting = Convert.ToDouble(doseResEntry);
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(doseEntry);
            }
            else
                MessageBox.Show("Inputs must all be numerical values!");
        }

        private void ClickPlot3(object sender, RoutedEventArgs e)
        {
            // Read input from textboxes
            var doseEntry = EnterDoseBox.Text;
            var spatialResEntry = SpatialResBox.Text;
            var doseResEntry = DoseResBox.Text;
            var xEntry = Box1.Text;
            var y1Entry = Box2.Text;
            var y2Entry = Box3.Text;

            // If searchres is defined too small the code might take too long to run
            if (IsDecimal(spatialResEntry))
            {
                double val = Convert.ToDouble(spatialResEntry);
                if (val < 0.001)
                {
                    MessageBox.Show("Spatial search grid resolution is set too low. Please input a value of at least 0.001.");
                    return;
                }
            }

            // Check if entries are numerical. If they are show plots.
            if (IsDecimal(doseEntry) & IsDecimal(spatialResEntry) & IsDecimal(doseResEntry) & IsDecimal(xEntry) & IsDecimal(y1Entry) & IsDecimal(y2Entry))
            {
                _vm.doseInput = Convert.ToDouble(doseEntry);
                _vm.spatialResInput = Convert.ToDouble(spatialResEntry);
                _vm.doseResInput = Convert.ToDouble(doseResEntry);
                _vm.lineXInput = Math.Round(Convert.ToDouble(xEntry), 2);
                Box1.Text = _vm.lineXInput.ToString();
                _vm.lineYInput = Math.Round(Convert.ToDouble(y1Entry), 2);
                Box2.Text = _vm.lineYInput.ToString();
                _vm.lowYInput = Math.Round(Convert.ToDouble(y2Entry), 2);
                Box3.Text = _vm.lowYInput.ToString();
                _vm.RefreshPlot3();

                // save values to user settings
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(spatialResEntry);
                Properties.Settings.Default.chosenDoseResSetting = Convert.ToDouble(doseResEntry);
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(doseEntry);

            }
            else
                MessageBox.Show("Inputs must all be numerical values!");
        }

        private void ClickPlot4(object sender, RoutedEventArgs e)
        {
            // Read input from textboxes
            var doseEntry = EnterDoseBox.Text;
            var spatialResEntry = SpatialResBox.Text;
            var doseResEntry = DoseResBox.Text;
            var xEntry = Box1.Text;
            var y1Entry = Box2.Text;
            var y2Entry = Box3.Text;

            // If searchres is defined too small the code might take too long to run
            if (IsDecimal(spatialResEntry))
            {
                double val = Convert.ToDouble(spatialResEntry);
                if (val < 0.001)
                {
                    MessageBox.Show("Spatial search grid resolution is set too low. Please input a value of at least 0.001.");
                    return;
                }
            }

            // Check if entries are numerical. If they are show plots.
            if (IsDecimal(doseEntry) & IsDecimal(spatialResEntry) & IsDecimal(doseResEntry) & IsDecimal(xEntry) & IsDecimal(y1Entry) & IsDecimal(y2Entry))
            {
                _vm.doseInput = Convert.ToDouble(doseEntry);
                _vm.spatialResInput = Convert.ToDouble(spatialResEntry);
                _vm.doseResInput = Convert.ToDouble(doseResEntry);
                _vm.lineXInput = Math.Round(Convert.ToDouble(xEntry), 2);
                Box1.Text = _vm.lineXInput.ToString();
                _vm.lineYInput = Math.Round(Convert.ToDouble(y1Entry), 2);
                Box2.Text = _vm.lineYInput.ToString();
                _vm.lowYInput = Math.Round(Convert.ToDouble(y2Entry), 2);
                Box3.Text = _vm.lowYInput.ToString();
                _vm.RefreshPlot4();

                // save values to user settings
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(spatialResEntry);
                Properties.Settings.Default.chosenDoseResSetting = Convert.ToDouble(doseResEntry);
                Properties.Settings.Default.chosenDoseSetting = Convert.ToDouble(doseEntry);

            }
            else
                MessageBox.Show("Inputs must all be numerical values!");
        }

        // Check if textbox input is numerical
        private bool IsDecimal(string input)
        {
            Decimal dummy;
            return Decimal.TryParse(input, out dummy);
        }
        #endregion

        #region structure selection logic
        private void FindSelectedStructure(object comboBoxObject, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)comboBoxObject;
            // read input from user settings
            var value = Properties.Settings.Default.chosenStructureSetting;
            if (_vm.Structures.Any(s => s.Id == value))
            { 
                comboBox.SelectedItem = _vm.Structures.FirstOrDefault(s => s.Id == value);
            }
            else
            {
                // Let the user know what went wrong.
                MessageBox.Show(string.Format("Saved structure could not be found in current structure set. Please choose a target structure from the drop down menu."));

                comboBox.SelectedItem = _vm.Structures.First();
            }
        }


        // Combo box selection logic
        private void StructureSelected(object comboBoxObject, RoutedEventArgs e)
        {
            // Remove all series from plot
            _vm.ClearAll();

            // set chosenID property in MainViewModel 
            var structure = GetStructure(comboBoxObject);
            _vm.ChosenStructureId = structure.Id;

            // Save selected structure id to user settings
            Properties.Settings.Default.chosenStructureSetting = structure.Id;

        }

        // Given combobox obect, find structure object
        private Structure GetStructure(object comboBoxObject)
        {
            var comboBox = (ComboBox)comboBoxObject;
            var structure = (Structure)comboBox.SelectedItem;
            return structure;
        }

        // Overload if given string ID
        private Structure GetStructure(string Id)
        {
            return _vm.Structures.First(s => s.Id == Id);
        }

        #endregion

        #region Export logic
        private void ExportPlot1AsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.firstViewModel.ExportPlotAsPdf(filePath);
        }

        private void ExportData1AsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.firstViewModel.ExportDataAsTxt(filePath);
        }

        private void ExportPlot2AsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.secondViewModel.ExportPlotAsPdf(filePath);
        }

        private void ExportData2AsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.secondViewModel.ExportDataAsTxt(filePath);
        }

        private void ExportPlot3AsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.thirdViewModel.ExportPlotAsPdf(filePath);
        }

        private void ExportData3AsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.thirdViewModel.ExportDataAsTxt(filePath);
        }

        private void ExportPlot4AsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.fourthViewModel.ExportPlotAsPdf(filePath);
        }

        private void ExportData4AsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.fourthViewModel.ExportDataAsTxt(filePath);
        }

        private void ExportPlot5AsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.concavenessViewModel.ExportPlotAsPdf(filePath);
        }

        private void ExportData6AsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.bodyDistViewModel.ExportDataAsTxt(filePath);
        }

        private void ExportPlot6AsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.bodyDistViewModel.ExportPlotAsPdf(filePath);
        }

        private void ExportData5AsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.concavenessViewModel.ExportDataAsTxt(filePath);
        }

        private void ExportDVHmetrics(object sender, RoutedEventArgs e)
        {
            var filePath = GetCsvSavePath();
            if (filePath != null)
                _vm.ExportDVHMetrics(filePath);
        }

        private string GetPdfSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export to PDF",
                Filter = "PDF Files (*.pdf)|*.pdf"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetTxtSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export plot data to txt file",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetCsvSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export DVH metrics to csv file",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetFolderSavePath()
        {
            var saveFileDialog = new CommonOpenFileDialog
            {
                Title = "Choose folder to save files in",
                IsFolderPicker = true
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == CommonFileDialogResult.Ok)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private void ExportAllAsTxt(object sender, RoutedEventArgs e)
        {
            string dir = GetFolderSavePath();
            if (dir != null)
            {
                _vm.firstViewModel.ExportDataAsTxt(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot1_" + _vm.lineXInput.ToString() + "_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".txt" }));

                _vm.secondViewModel.ExportDataAsTxt(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot2_" + Math.Round(_vm.lineYInput).ToString() + "_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".txt" }));

                _vm.thirdViewModel.ExportDataAsTxt(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot3_" + Math.Round(_vm.lowYInput).ToString() + "_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".txt" }));

                _vm.fourthViewModel.ExportDataAsTxt(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot4_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".txt" }));

                _vm.concavenessViewModel.ExportDataAsTxt(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_Concaveness.txt" }));

                _vm.bodyDistViewModel.ExportDataAsTxt(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_PTVtoBODY.txt" }));
            }
        }

        private void ExportAllAsPdf(object sender, RoutedEventArgs e)
        {
            string dir = GetFolderSavePath();
            if (dir != null)
            {
                _vm.firstViewModel.ExportPlotAsPdf(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot1_" + _vm.lineXInput.ToString() + "_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".pdf" }));

                _vm.secondViewModel.ExportPlotAsPdf(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot2_" + Math.Round(_vm.lineYInput).ToString() + "_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".pdf" }));

                _vm.thirdViewModel.ExportPlotAsPdf(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot3_" + Math.Round(_vm.lowYInput).ToString() + "_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".pdf" }));

                _vm.fourthViewModel.ExportPlotAsPdf(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_" + _vm.doseInput.ToString() + "Gy_Plot4_" + _vm.spatialResInput.ToString() + "_" + _vm.doseResInput.ToString() + ".pdf" }));

                _vm.concavenessViewModel.ExportPlotAsPdf(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_Concaveness.pdf" }));

                _vm.bodyDistViewModel.ExportPlotAsPdf(System.IO.Path.Combine(new string[] { dir, _vm.patientID + "_PTVtoBODY.pdf" }));
            }
        }
        #endregion 


    }
}
