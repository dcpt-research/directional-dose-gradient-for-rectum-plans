This script caluclates and displays information on the spatial dose distribution in rectum treatment plans. 
Specifically it calculates a dose gradient measure in AP or LR directions at user-specified coordinates and displays this along the CC axis.

The script is run as a binary plugin directly from the Eclipse interface.

Dependencies:
- ESAPI 15.6
- Oxyplot v. 2.0.0.0
- Oxyplot Wpf v. 2.0.0.0
- Oxyplot Pdf v. 2.0.0.0
- MoreLinq v. 3.3.2.0

Instructions for use:
In the following, coordinates are defined as: x - LR, y - AP, z - CC.
Run the script. A WPF window will appear. If the script has been run before previous settings (target, dose level) will be automatically loaded. 
If no saved settings can be found, or the current structure set does not contain the saved ID the user is asked to specify a target. Default evaluation dose is set to 40 Gy.
Select desired target structure and dose level (if needed) and click "Calculate and show".
Plot 1 shows gradients in the A and P directions, plots 2 and 3 in the L and R directions at different y-locations, and plot 4 shows the gradient measure in the A direction above the anterior-most points on the left and right sides of the target. This corresponds to the two "horns" of the PTV arising from lymph node inclusion.
The x- or y-coordinates for the lines along which the gradient measure is calculated in plots 1-3 can be changed independently. 
After changing a certain parameter, plots can be recalculated and refreshed separately if needed by clicking one of the four "Refresh plot X" buttons.

Plots can be exported as PDF files or as rxt files containing the actual data points. To do this, right click the desired plot.